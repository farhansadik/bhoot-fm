#!/bin/bash
#!/usr/bin/bash
#!/usr/bin/zsh

set import yt-dlp from /usr/bin/yt-dlp
set default package yt-dlp

# version=0.13 Alpha
# maya by dr alif
parent="https://www.youtube.com/@mayabhoutiggota/videos"
year=2024

# no protection has been implemented 
# no color has been added 
# no log system has been added 

function createDirectory() {
	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/Maya/$year/$1; then cd Bhoot\ FM/Maya/$year/$1; fi
	} fi
}

function sampleFunction() {
	
	# File Name Format
	# if 'special episode' = BS_DD_MM_YY
	# if 'basic episode' = epX_DD_MM_YY

	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M2_10-1-24.%(ext)s" https://www.youtube.com/watch?v=WT6FTmgrl18
}

function january() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M1_4-1-24.%(ext)s" https://www.youtube.com/watch?v=CG7nnsLTAN8
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M2_11-1-24.%(ext)s" https://www.youtube.com/watch?v=GuEt1KkPx0c
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M3_18-1-24.%(ext)s" https://www.youtube.com/watch?v=UscaYinE37c
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M4_25-1-24.%(ext)s" https://www.youtube.com/watch?v=Ibd5-QXUMGA
}

function february() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M5_1-2-24.%(ext)s" https://www.youtube.com/watch?v=jvW-ALlYwyY
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M6_8-2-24.%(ext)s" https://www.youtube.com/watch?v=D3NonsOVVoI
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M7_15-2-24.%(ext)s" https://www.youtube.com/watch?v=2CDNLKwPdXs
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "NISHIR_DAK_P1_22-2-24.%(ext)s" https://www.youtube.com/watch?v=dwWrw20FjpE
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "SHIHORON_P1_22-2-24.%(ext)s" https://www.youtube.com/watch?v=QaevYGu3kKA
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M8_23-2-24.%(ext)s" https://www.youtube.com/watch?v=7cgpT9cvXd8
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "NISHIR_DAK_P2_29-2-24.%(ext)s" https://www.youtube.com/watch?v=TFOgrq--CN0
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "SHIHORON_P2_29-2-24.%(ext)s" https://www.youtube.com/watch?v=j1iY3EaUi2w	
}

function march() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M9_1-3-24.%(ext)s" https://www.youtube.com/watch?v=7O1PaLSJWrI
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "X1_7-3-24.%(ext)s" https://www.youtube.com/watch?v=i1PuKdqMixc
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M10_7-3-24.%(ext)s" https://www.youtube.com/watch?v=8xQoB9C8knU
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "X2_14-3-24.%(ext)s" https://www.youtube.com/watch?v=zHtoj9jJYt8
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "X3_14-3-24.%(ext)s" https://www.youtube.com/watch?v=RYSpFe4EnFg
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M11_14-3-24.%(ext)s" https://www.youtube.com/watch?v=hVzgz5MMRnk
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "X4_21-3-24.%(ext)s" https://www.youtube.com/watch?v=nqjpxwGViFQ
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "X5_21-3-24.%(ext)s" https://www.youtube.com/watch?v=jXS9oVVHhPU
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M12_21-3-24.%(ext)s" https://www.youtube.com/watch?v=rQPMwSG_-xA
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "X6_28-3-24.%(ext)s" https://www.youtube.com/watch?v=7QHurGTi7V8
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "X7_28-3-24.%(ext)s" https://www.youtube.com/watch?v=8nBE9oFRnrc
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M13_28-3-24.%(ext)s" https://www.youtube.com/watch?v=Zt6l1DUWXqs
}

function april() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Chitashal_4-4-24.%(ext)s" https://www.youtube.com/watch?v=fPh-e8HthP0
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Shiharon_4-4-24.%(ext)s" https://www.youtube.com/watch?v=leedqyBF134
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M14_4-4-24.%(ext)s" https://www.youtube.com/watch?v=XPZOjh8HCT4
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M15_11-3-24.%(ext)s" https://www.youtube.com/watch?v=Hfru5kWd8lM
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Taranath Tantrik_EP1_12-4-24.%(ext)s" https://www.youtube.com/watch?v=OKx6Bjm4vuQ
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M16_12-4-24.%(ext)s" https://www.youtube.com/watch?v=p_n0Hyi8dng
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "RedRoom_13-4-24.%(ext)s" https://www.youtube.com/watch?v=pljHvJZI6zc
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Horir Hotel_24-4-24.%(ext)s" https://www.youtube.com/watch?v=4J5elB7BI4o
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M17_25-4-24.%(ext)s" https://www.youtube.com/watch?v=xYCPuNRse-I
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M18_26-4-24.%(ext)s" https://www.youtube.com/watch?v=ATLe26JmDDA
}

function may() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Tutankhamen_1-5-24.%(ext)s" https://www.youtube.com/watch?v=Gy8jZ9UEhpE
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M19_2-5-24.%(ext)s" https://www.youtube.com/watch?v=D38psoYiQqk
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M20_3-5-24.%(ext)s" https://www.youtube.com/watch?v=GDE7cw3d1qA
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Kalo Jadu_EP1_8-5-24.%(ext)s" https://www.youtube.com/watch?v=BBlMnf357kE
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M21_9-5-24.%(ext)s" https://www.youtube.com/watch?v=CXYhAlIznLM
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M22_10-5-24.%(ext)s" https://www.youtube.com/watch?v=CmAwOKhwa1c
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Kalo Jadu_EP2_15-5-24.%(ext)s" https://www.youtube.com/watch?v=so55_EKne8k
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M23_16-5-24.%(ext)s" https://www.youtube.com/watch?v=37YvLfRleSM
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M24_17-5-24.%(ext)s" https://www.youtube.com/watch?v=VEOkanaas6Y
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Prachin Mudra_EP1_22-5-24.%(ext)s" https://www.youtube.com/watch?v=oE2lAx14u2M
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M25_1_23-5-24.%(ext)s" https://www.youtube.com/watch?v=di748Wd5tXU
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M25_2_24-5-24.%(ext)s" https://www.youtube.com/watch?v=f8FlA-eMAYI
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Haat Kata Tantrik_29-5-24.%(ext)s" https://www.youtube.com/watch?v=cgiyOO7kDZA
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M26_30-5-24.%(ext)s" https://www.youtube.com/watch?v=EIj1dv1I-LQ
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M27_31-5-24.%(ext)s" https://www.youtube.com/watch?v=JShoQ8PsgJg
}

function june() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Prachin Mudra_EP2_5-6-24.%(ext)s" https://www.youtube.com/watch?v=v8oj4skpVbA
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M28_6-6-24.%(ext)s" https://www.youtube.com/watch?v=ozINFABbXEU
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M29_7-6-24.%(ext)s" https://www.youtube.com/watch?v=VX2NQjkjD6I
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Thogi_EP1_12-6-24.%(ext)s" https://www.youtube.com/watch?v=QJET1Em2v_0
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M30_13-6-24.%(ext)s" https://www.youtube.com/watch?v=9_idd2w-60Q
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M31_17-6-24.%(ext)s" https://www.youtube.com/watch?v=Wi5cdaJ_0tU
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M32_20-6-24.%(ext)s" https://www.youtube.com/watch?v=VAytK_vcb4s
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Thogi_EP2_26-6-24.%(ext)s" https://www.youtube.com/watch?v=BRcu0aOGBrw
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M33_27-6-24.%(ext)s" https://www.youtube.com/watch?v=Kni9dvBGbTg
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M34_28-6-24.%(ext)s" https://www.youtube.com/watch?v=RdnqgjXmWeE
}

function july() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Hakini_3-7-24.%(ext)s" https://www.youtube.com/watch?v=h2k1giJ-4ZM
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M35_4-7-24.%(ext)s" https://www.youtube.com/watch?v=31Uqon5BiOQ
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M36_5-7-24.%(ext)s" https://www.youtube.com/watch?v=-aZnY-QeuSA
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Koila Khoni_10-7-24.%(ext)s" https://www.youtube.com/watch?v=iNYSka5PvCI
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M37_11-7-24.%(ext)s" https://www.youtube.com/watch?v=a81xcyG9Nbs
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M38_12-7-24.%(ext)s" https://www.youtube.com/watch?v=Vczcc_LWw_o
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Bajrangi_EP1_17-7-24.%(ext)s" https://www.youtube.com/watch?v=H3uMFiDpWm4
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M39_26-7-24.%(ext)s" https://www.youtube.com/watch?v=61UPu-grHYA
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Bajrangi_EP2_31-7-24.%(ext)s" https://www.youtube.com/watch?v=QAstmLMQGio
}

function august() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M40_1-8-24.%(ext)s" https://www.youtube.com/watch?v=7r0VfRFxUd0
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Vadua Jam Jam_7-8-24.%(ext)s" https://www.youtube.com/watch?v=bxQYwqLoy14
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M41_8-8-24.%(ext)s" https://www.youtube.com/watch?v=jfa_-0pUbfc
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Manusher Gondho_EP1_14-8-24.%(ext)s" https://www.youtube.com/watch?v=eumLc3DvjxU
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M42_15-8-24.%(ext)s" https://www.youtube.com/watch?v=0HQyYgex1M8
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M43_16-8-24.%(ext)s" https://www.youtube.com/watch?v=LPpZoqPcSas
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Manusher Gondho_EP2_21-8-24.%(ext)s" https://www.youtube.com/watch?v=qVlT8Z61nBA
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M44_22-8-24.%(ext)s" https://www.youtube.com/watch?v=rWjg31csA_o
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Doc Phyco_EP1_23-8-24.%(ext)s" https://www.youtube.com/watch?v=kIcXvX_Yo8Q 
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Vooter Chosma_28-8-24.%(ext)s" https://www.youtube.com/watch?v=6cEq7ravsuM
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M45_29-8-24.%(ext)s" https://www.youtube.com/watch?v=zeTPrXkl62I
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Doc Phyco_EP2_30-8-24.%(ext)s" https://www.youtube.com/watch?v=7dglBeGpPFs
}

function september() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M46_5-9-24.%(ext)s" https://www.youtube.com/watch?v=eqK-ws1Duek
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Doc Phyco_EP3_6-8-24.%(ext)s" https://www.youtube.com/watch?v=9kPvH9DajB4
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M47_12-9-24.%(ext)s" https://www.youtube.com/watch?v=wi1_0WRlwB8
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Doc Phyco_EP4_13-8-24.%(ext)s" https://www.youtube.com/watch?v=Lu3RjGz18T4
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "M48_19-9-24.%(ext)s" https://www.youtube.com/watch?v=-Qky3nQn3qo
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Doc Phyco_EP5_20-8-24.%(ext)s" https://www.youtube.com/watch?v=5NufzNRRqjg
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "MiddleEast High Command_26-9-24.%(ext)s" https://www.youtube.com/watch?v=hbvZb4X7Hj0
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Doc Phyco_EP6_27-9-24.%(ext)s" https://www.youtube.com/watch?v=RVq_vFmq3C0
}

function october() {
	sleep 1
}

function november() {
	sleep 1
}

function december() {
	sleep 1
}

clear 
echo Maya $year
echo 
echo "[1]  January (1-4)"
echo "[2]  February (5-8) + (SE 4)"
echo "[3]  March (9-12) + (SE 8)"
echo "[4]  April (13-18) + (SE 5)"
echo "[5]  May (19-27) + (SE 5)"
echo "[6]  June (28-34) + (SE 3)"
echo "[7]  July (35-39) + (SE 4)"
echo "[8]  August (40-45) + (SE 6)"
echo "[9]  September (46-48) + (SE 5)"
#echo "[10] October ()"
#echo "[11] November ()"
#echo "[12] December ()"
echo "[0]  Exit"
echo 
read -p "select month : " input 
echo 

# format 
# if create_directory year month; then execute_functtion; fi
# if createDirectory 2020 January; then january; fi

if [[ $input == 1 ]]; then createDirectory January; january;
elif [[ $input == 2 ]]; then createDirectory February; february; 
elif [[ $input == 3 ]]; then createDirectory March; march; 
elif [[ $input == 4 ]]; then createDirectory April; april; 
elif [[ $input == 5 ]]; then createDirectory May; may; 
elif [[ $input == 6 ]]; then createDirectory June; june;
elif [[ $input == 7 ]]; then createDirectory July; july; 
elif [[ $input == 8 ]]; then createDirectory August; august;
elif [[ $input == 9 ]]; then createDirectory September; september;
elif [[ $input == 10 ]]; then createDirectory October; october; 
elif [[ $input == 11 ]]; then createDirectory November; november; 
elif [[ $input == 12 ]]; then createDirectory December; december;
elif [[ $input == 0 || $input == 'q' ]]; then exit 0
else echo please select between 1-12; echo; fi
exit 0