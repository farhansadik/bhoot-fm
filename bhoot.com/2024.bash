#!/bin/bash
#!/usr/bin/bash
#!/usr/bin/zsh

set import yt-dlp from /usr/bin/yt-dlp
set default package yt-dlp

# version=0.13 Alpha
# bhoot.fom by rj russel
parent="https://www.youtube.com/@rjrussell.bhootdotcom/videos"
year=2024

function createDirectory() {
	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/Bhoot.com/$year/$1; then cd Bhoot\ FM/Bhoot.com/$year/$1; fi
	} fi
}

function sampleFunction() {
	
	# File Name Format
	# if 'special episode' = BS_DD_MM_YY
	# if 'basic episode' = epX_DD_MM_YY

	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep2_10-1-20.%(ext)s" https://www.youtube.com/watch?v=WT6FTmgrl18
}

function january() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep87_4-1-24.%(ext)s" https://www.youtube.com/watch?v=gzcf6PMCW3Q
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep88_11-1-24.%(ext)s" https://www.youtube.com/watch?v=2t31TCzdB0w
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep89_18-1-24.%(ext)s" https://www.youtube.com/watch?v=zNTgZlxUd0s
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep90_25-1-24.%(ext)s" https://www.youtube.com/watch?v=u5BJkGVohnA
}

function february() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep91_1-2-24.%(ext)s" https://www.youtube.com/watch?v=AHpBs0CB4hw
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep92_8-2-24.%(ext)s" https://www.youtube.com/watch?v=FKDlF_Fsrko
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep93_15-2-24.%(ext)s" https://www.youtube.com/watch?v=TabyySgySvw
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep94_22-2-24.%(ext)s" https://www.youtube.com/watch?v=R96s5Caey20
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep95_29-2-24.%(ext)s" https://www.youtube.com/watch?v=LQdP2A2Qec8
}

function march() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep96_7-3-24.%(ext)s" https://www.youtube.com/watch?v=umcg7BqGEmE
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep97_14-3-24.%(ext)s" https://www.youtube.com/watch?v=VcanTpgDNJ8
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep98_21-3-24.%(ext)s" https://www.youtube.com/watch?v=4GlEveKtC68
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep99_28-3-24.%(ext)s" https://www.youtube.com/watch?v=enuKqtmI95M
}

function april() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep100_4-4-24.%(ext)s" https://www.youtube.com/watch?v=Josk2Z0ZakQ
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep101_10-4-24.%(ext)s" https://www.youtube.com/watch?v=u9Ni5VdIXhc
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep102_11-4-24.%(ext)s" https://www.youtube.com/watch?v=YLcwYMbfLvQ
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep103_18-4-24.%(ext)s" https://www.youtube.com/watch?v=Kc-_xEr5ctk
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep104_25-4-24.%(ext)s" https://www.youtube.com/watch?v=-QnF_bry7Bk

}

function may() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep105_2-5-24.%(ext)s" https://www.youtube.com/watch?v=iLIHASxQWn0
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep106_9-5-24.%(ext)s" https://www.youtube.com/watch?v=7daf3P_JZ70
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep107_16-5-24.%(ext)s" https://www.youtube.com/watch?v=cV4n_BdHMjU
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep108_23-5-24.%(ext)s" https://www.youtube.com/watch?v=cV4n_BdHMjU
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep109_30-5-24.%(ext)s" https://www.youtube.com/watch?v=vIL3eJsREgE
}

function june() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep110_6-6-24.%(ext)s" https://www.youtube.com/watch?v=oicVrywuw1Q
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep111_13-6-24.%(ext)s" https://www.youtube.com/watch?v=xv6o7oerC0k
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep112_15-6-24.%(ext)s" https://www.youtube.com/watch?v=_4gzjvdpqPM
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep113_15-6-24.%(ext)s" https://www.youtube.com/watch?v=sCCBqPKtnuE
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep114_17-6-24.%(ext)s" https://www.youtube.com/watch?v=AOJ9a9BE044
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep115_18-6-24.%(ext)s" https://www.youtube.com/watch?v=Ojtfqc3RQjY
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep116_19-6-24.%(ext)s" https://www.youtube.com/watch?v=t73joUZrhAQ
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep117_20-6-24.%(ext)s" https://www.youtube.com/watch?v=FAj7vxVy5AA
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep118_27-6-24.%(ext)s" https://www.youtube.com/watch?v=ohgDwFmEH2E
}

function july() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep119_2-7-24.%(ext)s" https://www.youtube.com/watch?v=B0YOBvsfNPk
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep120_4-7-24.%(ext)s" https://www.youtube.com/watch?v=6hI8hR_9_Yw
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep121_6-7-24.%(ext)s" https://www.youtube.com/watch?v=RiTtM284U_4
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep122_7-7-24.%(ext)s" https://www.youtube.com/watch?v=7K2aVV0suvk
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep124_11-7-24.%(ext)s" https://www.youtube.com/watch?v=-btLPvB4nsA
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep125_12-7-24.%(ext)s" https://www.youtube.com/watch?v=EKKyrZsmsYs
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep126_28-7-24.%(ext)s" https://www.youtube.com/watch?v=v0KL6kM_I4c
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep127_30-7-24.%(ext)s" https://www.youtube.com/watch?v=DKrQJ5sSXAw
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep128_31-7-24.%(ext)s" https://www.youtube.com/watch?v=lOBFIs7OJog
}

function august() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep129_1-8-24.%(ext)s" https://www.youtube.com/watch?v=nGC6EN_XK5Y
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep130_2-8-24.%(ext)s" https://www.youtube.com/watch?v=fs7ePUrXFWc
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep131_8-8-24.%(ext)s" https://www.youtube.com/watch?v=Ho9A5ePKRo4
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep132_9-8-24.%(ext)s" https://www.youtube.com/watch?v=7y01AViuY0A
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep133_11-8-24.%(ext)s" https://www.youtube.com/watch?v=TM-lMunXthk
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep134_15-8-24.%(ext)s" https://www.youtube.com/watch?v=o4_oJc4QxAU
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep135_16-8-24.%(ext)s" https://www.youtube.com/watch?v=xTCoEKek5OI
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep136_22-8-24.%(ext)s" https://www.youtube.com/watch?v=_UZLtrW7aMc
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep137_23-8-24.%(ext)s" https://www.youtube.com/watch?v=RbMiiZgE2ZM
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep138_29-8-24.%(ext)s" https://www.youtube.com/watch?v=sj0NXDNUIoY
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep139_30-8-24.%(ext)s" https://www.youtube.com/watch?v=FxHx2ibsb4g
}

function september() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep140_5-9-24.%(ext)s" https://www.youtube.com/watch?v=k4vCC4cX2mk
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep141_6-9-24.%(ext)s" https://www.youtube.com/watch?v=4xzk319uh6U
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep142_12-9-24.%(ext)s" https://www.youtube.com/watch?v=OSk1RFfLzdU
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep143_13-9-24.%(ext)s" https://www.youtube.com/watch?v=kNVq-Bq6-Gg
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep144_19-9-24.%(ext)s" https://www.youtube.com/watch?v=_k1Gayidbgk
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep145_21-9-24.%(ext)s" https://www.youtube.com/watch?v=D8ZWRpT062E
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep146_24-9-24.%(ext)s" https://www.youtube.com/watch?v=AQkXw-O9V5U
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep147_26-9-24.%(ext)s" https://www.youtube.com/watch?v=VRrOK3pHZbA
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep148_27-9-24.%(ext)s" https://www.youtube.com/watch?v=4xJINutqIYw
}

function october() {
	sleep 1
}

function november() {
	sleep 1
}

function december() {
	sleep 1
}

clear 
echo Odvootoore $year
echo 
echo "[1]  January (87-90)"
echo "[2]  February (91-95)"
echo "[3]  March (96-99)"
echo "[4]  April (100-104)"
echo "[5]  May (105-109)"
echo "[6]  June (110-118)"
echo "[7]  July (119-128)"
echo "[8]  August (129-139)"
echo "[9]  September (140-148)"
#echo "[10] October ()"
#echo "[11] November ()"
#echo "[12] December ()"
echo "[0]  Exit"
echo 
read -p "select month : " input 
echo 

# format 
# if create_directory year month; then execute_functtion; fi
# if createDirectory 2020 January; then january; fi

if [[ $input == 1 ]]; then createDirectory January; january;
elif [[ $input == 2 ]]; then createDirectory February; february; 
elif [[ $input == 3 ]]; then createDirectory March; march; 
elif [[ $input == 4 ]]; then createDirectory April; april; 
elif [[ $input == 5 ]]; then createDirectory May; may; 
elif [[ $input == 6 ]]; then createDirectory June; june;
elif [[ $input == 7 ]]; then createDirectory July; july; 
elif [[ $input == 8 ]]; then createDirectory August; august;
elif [[ $input == 9 ]]; then createDirectory September; september;
elif [[ $input == 10 ]]; then createDirectory October; october; 
elif [[ $input == 11 ]]; then createDirectory November; november; 
elif [[ $input == 12 ]]; then createDirectory December; december;
elif [[ $input == 0 || $input == 'q' ]]; then exit 0
else echo please select between 1-12; echo; fi
exit 0

