#!/bin/bash
#!/usr/bin/bash
#!/usr/bin/zsh

set import yt-dlp from /usr/bin/yt-dlp
set default package yt-dlp

# version=0.13 Alpha
# bhoot.fom by rj russel
parent="https://www.youtube.com/@rjrussell.bhootdotcom/videos"
year=2023

function createDirectory() {
	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/Bhoot.com/$year/$1; then cd Bhoot\ FM/Bhoot.com/$year/$1; fi
	} fi
}

function january() {
	sleep 1	
}

function february() {
	sleep 1
}

function march() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep45-1_2-3-23.%(ext)s" https://www.youtube.com/watch?v=WtiMbHew0MY
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep45-2_3-3-23.%(ext)s" https://www.youtube.com/watch?v=U6kMb9xAppU
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep45-3_3-3-23.%(ext)s" https://www.youtube.com/watch?v=jTOka5L2VVY
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep46_9-3-23.%(ext)s" https://www.youtube.com/watch?v=B61t5loaaQw
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep47_16-3-23.%(ext)s" https://www.youtube.com/watch?v=Jo6cFhrknnE
}

function april() {
	sleep 1
}

function may() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep52_4-5-23.%(ext)s" https://www.youtube.com/watch?v=SXb5VfwcmK0
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep53_11-5-23.%(ext)s" https://www.youtube.com/watch?v=pOzZfFIMsa4
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep54_18-5-23.%(ext)s" https://www.youtube.com/watch?v=qwnw91l3WFk
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep55_25-5-23.%(ext)s" https://www.youtube.com/watch?v=TpiDp3S1xWo
}

function june() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep56_1-6-23.%(ext)s" https://www.youtube.com/watch?v=Tk7PlozhnkY
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep57_8-6-23.%(ext)s" https://www.youtube.com/watch?v=-8yifXLYoI4
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep58_16-6-23.%(ext)s" https://www.youtube.com/watch?v=X83U_f6oduM
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep59_22-6-23.%(ext)s" https://www.youtube.com/watch?v=jHNvPR86Y0I
}

function july() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep60_1-7-23.%(ext)s" https://www.youtube.com/watch?v=9aVZBO-YK-Y
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep61_6-7-23.%(ext)s" https://www.youtube.com/watch?v=voJ-YMei9OQ
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep62_14-7-23.%(ext)s" https://www.youtube.com/watch?v=sGp2JzF4NmI
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep63_20-7-23.%(ext)s" https://www.youtube.com/watch?v=PwaDsscFvf0 
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep64_27-7-23.%(ext)s" https://www.youtube.com/watch?v=_nBVz6BrCOg
}

function august() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep65_3-8-23.%(ext)s" https://www.youtube.com/watch?v=eSVhBpYtr28
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep66_10-8-23.%(ext)s" https://www.youtube.com/watch?v=0dj_Eg6gWFU
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep67_17-8-23.%(ext)s" https://www.youtube.com/watch?v=nLO1pCnJMTI
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep68_24-8-23.%(ext)s" https://www.youtube.com/watch?v=mnMZKEFAcgc
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep69_31-8-23.%(ext)s" https://www.youtube.com/watch?v=trDG9BWtx50
}
function september() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep70_7-9-23.%(ext)s" https://www.youtube.com/watch?v=G38i5FG1lLs
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep71_15-9-23.%(ext)s" https://www.youtube.com/watch?v=LEoYn8nipco
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep72_21-9-23.%(ext)s" https://www.youtube.com/watch?v=Wu3ezvRxQng
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep73_28-9-23.%(ext)s" https://www.youtube.com/watch?v=A0XUw1KkBuc
}

function october() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep74_5-10-23.%(ext)s" https://www.youtube.com/watch?v=6FWi0XD-a0A
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep75_12-10-23.%(ext)s" https://www.youtube.com/watch?v=US1PO4eEGD8
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep76_19-10-23.%(ext)s" https://www.youtube.com/watch?v=88xIs73EAx0
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep77_26-10-23.%(ext)s" https://www.youtube.com/watch?v=Nll4kDgeoPM
}

function november() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep78_2-11-23.%(ext)s" https://www.youtube.com/watch?v=1FfqWdr4jU4
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep79_9-11-23.%(ext)s" https://www.youtube.com/watch?v=QPCg-R64caw
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep80_16-11-23.%(ext)s" https://www.youtube.com/watch?v=l0xWLdR5iNE
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep81_23-11-23.%(ext)s" https://www.youtube.com/watch?v=ivjPVuqX-y8
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep82_30-11-23.%(ext)s" https://www.youtube.com/watch?v=sgPB0h9At5I
}

function december() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep83_7-12-23.%(ext)s" https://www.youtube.com/watch?v=nGoW-ShtjAQ
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep84_14-12-23.%(ext)s" https://www.youtube.com/watch?v=1Iz8dY9H3J4
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep85_21-12-23.%(ext)s" https://www.youtube.com/watch?v=cvWMUFBpNpY
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep86_28-12-23.%(ext)s" https://www.youtube.com/watch?v=pDibUkwtAeI
}

clear 
echo Bhoot.com $year
echo 
#echo "[1]  January ()"
#echo "[2]  February ()"
echo "[3]  March (45-47)"
#echo "[4]  April ()"
echo "[5]  May (52-55)"
echo "[6]  June (56-59)"
echo "[7]  July (60-64)"
echo "[8]  August (65-69)"
echo "[9]  September (70-63)"
echo "[10] October (74-77)"
echo "[11] November (78-81)"
echo "[12] December (82-86)"
echo "[0]  Exit"
echo 
read -p "select month : " input 
echo 

# format 
# if create_directory year month; then execute_functtion; fi
# if createDirectory 2020 January; then january; fi

if [[ $input == 1 ]]; then createDirectory January; january;
elif [[ $input == 2 ]]; then createDirectory February; february; 
elif [[ $input == 3 ]]; then createDirectory March; march; 
elif [[ $input == 4 ]]; then createDirectory April; april; 
elif [[ $input == 5 ]]; then createDirectory May; may; 
elif [[ $input == 6 ]]; then createDirectory June; june;
elif [[ $input == 7 ]]; then createDirectory July; july; 
elif [[ $input == 8 ]]; then createDirectory August; august;
elif [[ $input == 9 ]]; then createDirectory September; september;
elif [[ $input == 10 ]]; then createDirectory October; october; 
elif [[ $input == 11 ]]; then createDirectory November; november; 
elif [[ $input == 12 ]]; then createDirectory December; december;
elif [[ $input == 0 || $input == 'q' ]]; then exit 0
else echo please select between 1-12; echo; fi
exit 0
