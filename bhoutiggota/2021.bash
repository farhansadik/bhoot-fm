#!/bin/bash
#!/usr/bin/bash
#!/usr/bin/zsh

set import yt-dlp from /usr/bin/yt-dlp
set default package yt-dlp

# version=0.13 Alpha
# bhoutiggota by dr alif 
parent="https://www.youtube.com/c/Bhoutiggota/videos"
year=2021

# no protection has been implemented 
# no color has been added 
# no log system has been added 

function createDirectory() {
	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/Bhoutiggota/$year/$1; then cd Bhoot\ FM/Bhoutiggota/$year/$1; fi
	} fi
}

function sampleFunction() {
	
	# File Name Format
	# if 'special episode' = BS_DD_MM_YY
	# if 'basic episode' = epX_DD_MM_YY

	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B1_30-1-21.%(ext)s" https://www.youtube.com/watch?v=ymYB4d6_w9E
}

function january() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B52_8-1-21.%(ext)s" https://www.youtube.com/watch?v=JSmLGwhYoas
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B53_15-1-21.%(ext)s" https://www.youtube.com/watch?v=QORCJyAqbOY
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B54_22-1-21.%(ext)s" https://www.youtube.com/watch?v=iPFu1iIl8W4
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B55_29-1-21.%(ext)s" https://www.youtube.com/watch?v=4JEd1EecalQ
}

function february() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B56_2-2-21.%(ext)s" https://www.youtube.com/watch?v=rd3m1ap-phQ
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B57_12-2-21.%(ext)s" https://www.youtube.com/watch?v=S7_FoSq9QPY
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B58_19-2-21.%(ext)s" https://www.youtube.com/watch?v=Sgb-Ffm3Yvg
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B59_26-2-21.%(ext)s" https://www.youtube.com/watch?v=EwClNlThMpg
}

function march() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B60_5-3-21.%(ext)s" https://www.youtube.com/watch?v=N_kiGkk1ONQ
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B61_12-3-21.%(ext)s" https://www.youtube.com/watch?v=ksOwLn-LdtM
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B62_19-3-21.%(ext)s" https://www.youtube.com/watch?v=g5DFhSDMwLI
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B63_26-3-21.%(ext)s" https://www.youtube.com/watch?v=YMzPcnEuMFo
}

function april() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B64_2-4-21.%(ext)s" https://www.youtube.com/watch?v=spEI34auENY
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B65_9-4-21.%(ext)s" https://www.youtube.com/watch?v=q69LyTaz-0w
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B66_16-4-21.%(ext)s" https://www.youtube.com/watch?v=0wuRzsBauss
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B67_23-4-21.%(ext)s" https://www.youtube.com/watch?v=u_V1ipIKA7A
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B68_30-4-21.%(ext)s" https://www.youtube.com/watch?v=KjHjNrg5Qmk
} 

function may() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B69_7-5-21.%(ext)s" https://www.youtube.com/watch?v=Lw2BV0bhMxc
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B70_15-5-21.%(ext)s" https://www.youtube.com/watch?v=-mTSaOd7ruQ
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B71_16-5-21.%(ext)s" https://www.youtube.com/watch?v=7Z8qho3ya3I
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B71_17-5-21.%(ext)s" https://www.youtube.com/watch?v=EmzjqsqT9hs
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B72_21-5-21.%(ext)s" https://www.youtube.com/watch?v=3TczgJ9sXSY
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B73_28-5-21.%(ext)s" https://www.youtube.com/watch?v=mLPrS0eJAkc
}

function june() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B74_4-6-21.%(ext)s" https://www.youtube.com/watch?v=IEgNj6JNvrs
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B75_11-6-21.%(ext)s" https://www.youtube.com/watch?v=uK_dUUJYU6Y
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B76_18-6-21.%(ext)s" https://www.youtube.com/watch?v=nvbxQ27yxdI
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B77_25-6-21.%(ext)s" https://www.youtube.com/watch?v=ARcA_8U0TmQ
}

function july() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B78_2-7-21.%(ext)s" https://www.youtube.com/watch?v=UfsTHs-y_po
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B79_9-7-21.%(ext)s" https://www.youtube.com/watch?v=Q917uw9hr7M
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B80_16-7-21.%(ext)s" https://www.youtube.com/watch?v=D2TbhUSdxg8
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B81_22-7-21.%(ext)s" https://www.youtube.com/watch?v=bWHz1ktDOek
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B82_23-7-21.%(ext)s" https://www.youtube.com/watch?v=6aMoDK5PywE
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B83_30-7-21.%(ext)s" https://www.youtube.com/watch?v=bNU3fvUvplc
}

function august() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B84_6-8-21.%(ext)s" https://www.youtube.com/watch?v=t5OOjf1iOuI
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B85_13-8-21.%(ext)s" https://www.youtube.com/watch?v=kdD0sLUOLkU
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B86_20-8-21.%(ext)s" https://www.youtube.com/watch?v=-RTDGJGw3JE
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B87_27-8-21.%(ext)s" https://www.youtube.com/watch?v=Sknugd-Lr9c
}

function september() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B88_3-9-21.%(ext)s" https://www.youtube.com/watch?v=y5WKgelMG9s
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B89_10-9-21.%(ext)s" https://www.youtube.com/watch?v=ABEqPvhtFyU
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B90_17-9-21.%(ext)s" https://www.youtube.com/watch?v=6pSdiV-1RMM
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B91_24-9-21.%(ext)s" https://www.youtube.com/watch?v=h75agB98Tmo
}

function october() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B92_1-10-21.%(ext)s" https://www.youtube.com/watch?v=TW-OK0aYhHU
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B93_8-10-21.%(ext)s" https://www.youtube.com/watch?v=pqI18rhVYEo
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B94_15-10-21.%(ext)s" https://www.youtube.com/watch?v=TQTTYxqAr1U
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B95_22-10-21.%(ext)s" https://www.youtube.com/watch?v=30UwcmLeRmI
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B96_29-10-21.%(ext)s" https://www.youtube.com/watch?v=toME9MFwdDs
}

function november() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B97_5-11-21.%(ext)s" https://www.youtube.com/watch?v=s7Gx7-JlMmA
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B98_12-11-21.%(ext)s" https://www.youtube.com/watch?v=y5XfaAu4B3k
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B99_19-11-21.%(ext)s" https://www.youtube.com/watch?v=F8tntCneQLk
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B100_26-11-21.%(ext)s" https://www.youtube.com/watch?v=OCWSwmy3kbY
}

function december() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B101_3-12-21.%(ext)s" https://www.youtube.com/watch?v=oYIWnzU60rw
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B102_13-12-21.%(ext)s" https://www.youtube.com/watch?v=9ij1lh3N4Dw
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B103_17-12-21.%(ext)s" https://www.youtube.com/watch?v=uIFMufTiJ3M
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B104_24-12-21.%(ext)s" https://www.youtube.com/watch?v=wUcLoOk9tM0
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B105_31-12-21.%(ext)s" https://www.youtube.com/watch?v=bHIeP0cLIdM
}

clear 
echo Bhoutiggota $year
echo 
echo "[1]  January (52-55)"
echo "[2]  February (56-59)"
echo "[3]  March (60-63)"
echo "[4]  April (64-68)"
echo "[5]  May (69-73)"
echo "[6]  June (74-77)"
echo "[7]  July (78-83)"
echo "[8]  August (84-87)"
echo "[9]  September (88-91)"
echo "[10] October (92-96)"
echo "[11] November (97-100)"
echo "[12] December (101-105)"
echo "[0]  Exit"
echo 
read -p "select month : " input 
echo 

# format 
# if create_directory year month; then execute_functtion; fi
# if createDirectory 2020 January; then january; fi

if [[ $input == 1 ]]; then createDirectory January; january;
elif [[ $input == 2 ]]; then createDirectory February; february; 
elif [[ $input == 3 ]]; then createDirectory March; march; 
elif [[ $input == 4 ]]; then createDirectory April; april; 
elif [[ $input == 5 ]]; then createDirectory May; may; 
elif [[ $input == 6 ]]; then createDirectory June; june;
elif [[ $input == 7 ]]; then createDirectory July; july; 
elif [[ $input == 8 ]]; then createDirectory August; august;
elif [[ $input == 9 ]]; then createDirectory September; september;
elif [[ $input == 10 ]]; then createDirectory October; october; 
elif [[ $input == 11 ]]; then createDirectory November; november; 
elif [[ $input == 12 ]]; then createDirectory December; december;
elif [[ $input == 0 || $input == 'q' ]]; then exit 0
else echo please select between 1-12; echo; fi
exit 0

# Farhan Sadik
