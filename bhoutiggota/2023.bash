#!/bin/bash
#!/usr/bin/bash
#!/usr/bin/zsh

set import yt-dlp from /usr/bin/yt-dlp
set default package yt-dlp

# version=0.12 Alpha
# bhoutiggota by dr alif 
parent="https://www.youtube.com/c/Bhoutiggota/videos"
year=2023

function createDirectory() {
	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/Bhoutiggota/$year/$1; then cd Bhoot\ FM/Bhoutiggota/$year/$1; fi
	} fi
}

function january() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "BX_4-1-23.%(ext)s" https://www.youtube.com/watch?v=4W-OheXNrH4
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B160_6-1-23.%(ext)s" https://www.youtube.com/watch?v=dyvC2mHibr0
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "BXP1_11-1-23.%(ext)s" https://www.youtube.com/watch?v=vIaGOZNocr4
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B161_13-1-23.%(ext)s" https://www.youtube.com/watch?v=9HmU5fjvxKM
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "BXP2_18-1-23.%(ext)s" https://www.youtube.com/watch?v=viH5_j90vTc
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B162_20-1-23.%(ext)s" https://www.youtube.com/watch?v=pUScbuHYxU8
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "BXP3_25-1-23.%(ext)s" https://www.youtube.com/watch?v=8WhorO59Mgc
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B163_27-1-23.%(ext)s" https://www.youtube.com/watch?v=rzeidM4rTTU
}

function february() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "BX_1-2-23.%(ext)s" https://www.youtube.com/watch?v=8aq0osV0cFg
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B164_3-2-23.%(ext)s" https://www.youtube.com/watch?v=hjMGxswfMS0
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "BXP1_8-2-23.%(ext)s" https://www.youtube.com/watch?v=g3MB9Tr4i2U
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B165_10-2-23.%(ext)s" https://www.youtube.com/watch?v=5TwNTtl7re0
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "BXP2_15-2-23.%(ext)s" https://www.youtube.com/watch?v=6OqCHOb-ugY
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B166_17-2-23.%(ext)s" https://www.youtube.com/watch?v=l7rEcOJF20s
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "BXP3_22-2-23.%(ext)s" https://www.youtube.com/watch?v=_tL4g1PRjmQ
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B167_24-2-23.%(ext)s" https://www.youtube.com/watch?v=sfb7ethYtaE
}

function march() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "BXP1_1-3-23.%(ext)s" https://www.youtube.com/watch?v=-BZ1mATl84c
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B168_3-3-23.%(ext)s" https://www.youtube.com/watch?v=aTjl-bl6DQ0
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "BXP2_8-3-23.%(ext)s" https://www.youtube.com/watch?v=JVHDX0wij_w
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B169_10-3-23.%(ext)s" https://www.youtube.com/watch?v=drZfWg4Nl_4
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "BXP3_15-3-23.%(ext)s" https://www.youtube.com/watch?v=WBFdJAd5Ya4
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B170_17-3-23.%(ext)s" https://www.youtube.com/watch?v=iRu1__Ebu64
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "BXP4_22-3-23.%(ext)s" https://www.youtube.com/watch?v=A7ey9dLonag
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B171_24-3-23.%(ext)s" https://www.youtube.com/watch?v=Hz7rgq9VPJ0
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "BKMP1_29-3-23.%(ext)s" https://www.youtube.com/watch?v=A5c8OWwqzZ0
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B172_31-3-23.%(ext)s" https://www.youtube.com/watch?v=r5_f2TdXaag

}

function april() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "BKMP2_5-4-23.%(ext)s" https://www.youtube.com/watch?v=BczrbkdioIA
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B173_7-4-23.%(ext)s" https://www.youtube.com/watch?v=HPxPyKlmIy8
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "BKMP3_12-4-23.%(ext)s" https://www.youtube.com/watch?v=A2UIrayGNGc
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B174_13-4-23.%(ext)s" https://www.youtube.com/watch?v=PA_Qf90O9ts
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "BXP1_14-4-23.%(ext)s" https://www.youtube.com/watch?v=jdo5MK2_08M
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B175_23-4-23.%(ext)s" https://www.youtube.com/watch?v=pekJb-2n47w
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B176_28-4-23.%(ext)s" https://www.youtube.com/watch?v=tCkQgtqdj2A
}

function may() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B177_1_5-5-23.%(ext)s" https://www.youtube.com/watch?v=Y5aC2RjHiU4
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B177_2_6-5-23.%(ext)s" https://www.youtube.com/watch?v=FInp1UXGPos
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B178_1_12-5-23.%(ext)s" https://www.youtube.com/watch?v=odveEe1xSLw
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B178_2_13-5-23.%(ext)s" https://www.youtube.com/watch?v=flZXiTAV99I
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B179_1_18-5-23.%(ext)s" https://www.youtube.com/watch?v=VBl0lufjl84
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B179_2_19-5-23.%(ext)s" https://www.youtube.com/watch?v=AIfktEtIiAY
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B180_25-5-23.%(ext)s" https://www.youtube.com/watch?v=rsCvFdO1uRA
}

function june() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B181_1-6-23.%(ext)s" https://www.youtube.com/watch?v=-_VcCRvdvW4
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B182_8-6-23.%(ext)s" https://www.youtube.com/watch?v=YHq3ly08rtU
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B183_15-6-23.%(ext)s" https://www.youtube.com/watch?v=By8s_iJu3H8
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B184_22-6-23.%(ext)s" https://www.youtube.com/watch?v=aabMCd0tFXg
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B185_29-6-23.%(ext)s" https://www.youtube.com/watch?v=5-aUkfoQoIU
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B186_30-6-23.%(ext)s" https://www.youtube.com/watch?v=oxwO5sRMef8
}

function july() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B187_6-7-23.%(ext)s" https://www.youtube.com/watch?v=YP9RlrWt4Tg
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B188_13-7-23.%(ext)s" https://www.youtube.com/watch?v=K4Kcf56X4Q0
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B189_20-7-23.%(ext)s" https://www.youtube.com/watch?v=mU48PW_eqwE
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B190_27-7-23.%(ext)s" https://www.youtube.com/watch?v=EPXcFQyI6PY
}

function august() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B191_3-8-23.%(ext)s" https://www.youtube.com/watch?v=3pPbobEeiUA
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B192_10-8-23.%(ext)s" https://www.youtube.com/watch?v=fPRdxjaVx7E
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B193_17-8-23.%(ext)s" https://www.youtube.com/watch?v=Z31zxjSHttY
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B194_24-8-23.%(ext)s" https://www.youtube.com/watch?v=DHVz7M_C8Yc
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B195_31-8-23.%(ext)s" https://www.youtube.com/watch?v=TZOsyBAiN7c
}

function september() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B196_7-9-23.%(ext)s" https://www.youtube.com/watch?v=YbQ__4Xax0Y
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B197_14-9-23.%(ext)s" https://www.youtube.com/watch?v=8B6whzJ-y_g
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B198_21-9-23.%(ext)s" https://www.youtube.com/watch?v=34H64B-OZyY
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B199_28-9-23.%(ext)s" https://www.youtube.com/watch?v=5TSqaSCtyLs
}

function october() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B200_5-10-23.%(ext)s" https://www.youtube.com/watch?v=d2Osk-vd_P4
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B201_12-10-23.%(ext)s" https://www.youtube.com/watch?v=SabEAvdMKS0
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B202_19-10-23.%(ext)s" https://www.youtube.com/watch?v=cXjSTG4bFa8
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B203_26-10-23.%(ext)s" https://www.youtube.com/watch?v=TRbPuDYRVrg
}

function november() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B204_2-11-23.%(ext)s" https://www.youtube.com/watch?v=Bxiy4mFDyhQ
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B205_9-11-23.%(ext)s" https://www.youtube.com/watch?v=AVrXI06HOFI
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B206_16-11-23.%(ext)s" https://www.youtube.com/watch?v=YIRYSAJ4Lug
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B207_23-11-23.%(ext)s" https://www.youtube.com/watch?v=izahEIx3mtw
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B208_30-11-23.%(ext)s" https://www.youtube.com/watch?v=LS0x5CfgWOI
}

function december() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B209_7-12-23.%(ext)s" https://www.youtube.com/watch?v=Byc41sOmevE
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B210_14-12-23.%(ext)s" https://www.youtube.com/watch?v=K-5T0n_UZxs
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B211_21-12-23.%(ext)s" https://www.youtube.com/watch?v=w1kteZaIS_w
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B212_31-12-23.%(ext)s" https://www.youtube.com/watch?v=5ZjaRNaIz9g
}


clear 
echo Bhoutiggota $year
echo 
echo "[1]  January (160-163)"
echo "[2]  February (164-167)"
echo "[3]  March (168-172)"
echo "[4]  April (173-176)"
echo "[5]  May (177-180)"
echo "[6]  June (181-186)"
echo "[7]  July (187-190)"
echo "[8]  August (191-195)"
echo "[9]  September (196-199)"
echo "[10] October (200-203)"
echo "[11] November (204-208)"
echo "[12] December (209-212)"
echo "[0]  Exit"
echo 
read -p "select month : " input 
echo 

# format 
# if create_directory year month; then execute_functtion; fi
# if createDirectory 2020 January; then january; fi

if [[ $input == 1 ]]; then createDirectory January; january;
elif [[ $input == 2 ]]; then createDirectory February; february; 
elif [[ $input == 3 ]]; then createDirectory March; march; 
elif [[ $input == 4 ]]; then createDirectory April; april; 
elif [[ $input == 5 ]]; then createDirectory May; may; 
elif [[ $input == 6 ]]; then createDirectory June; june;
elif [[ $input == 7 ]]; then createDirectory July; july; 
elif [[ $input == 8 ]]; then createDirectory August; august;
elif [[ $input == 9 ]]; then createDirectory September; september;
elif [[ $input == 10 ]]; then createDirectory October; october; 
elif [[ $input == 11 ]]; then createDirectory November; november; 
elif [[ $input == 12 ]]; then createDirectory December; december;
elif [[ $input == 0 || $input == 'q' ]]; then exit 0
else echo please select between 1-12; echo; fi
exit 0