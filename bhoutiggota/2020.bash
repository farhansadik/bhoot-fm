#!/bin/bash
#!/usr/bin/bash
#!/usr/bin/zsh

set import yt-dlp from /usr/bin/yt-dlp
set default package yt-dlp

# version=0.12 Alpha
# bhoutiggota by dr alif 
parent="https://www.youtube.com/c/Bhoutiggota/videos"
# last - 26 // 23-7-20

# no protection has been implemented 
# no color has been added 
# no log system has been added 

function createDirectory() {
	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/Bhoutiggota/$1/$2; then cd Bhoot\ FM/Bhoutiggota/$1/$2; fi
	} fi
}

function january() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B1_30-1-20.%(ext)s" https://www.youtube.com/watch?v=ymYB4d6_w9E
}

function february() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B2_6-2-20.%(ext)s" https://www.youtube.com/watch?v=wIOCLa18nJs
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B3_14-2-20.%(ext)s" https://www.youtube.com/watch?v=2_inK9ObrbQ
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B4_21-2-20.%(ext)s" https://www.youtube.com/watch?v=VOrRjDoHeHY
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B5_27-2-20.%(ext)s" https://www.youtube.com/watch?v=Xk5jeOf2u5c
}

function march() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B6_5-3-20.%(ext)s" https://www.youtube.com/watch?v=mF_KJPgNA64
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B7_12-3-20.%(ext)s" https://www.youtube.com/watch?v=zPzhnbqB-OA
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "BS_13-3-20.%(ext)s" https://www.youtube.com/watch?v=QAy_sIMq2KM
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B8_19-3-20.%(ext)s" https://www.youtube.com/watch?v=SAQh-g2Nxso
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B9_26-3-20.%(ext)s" https://www.youtube.com/watch?v=rmZk6_mXofc
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "BS_28-3-20.%(ext)s" https://www.youtube.com/watch?v=cVX9Pcm369U
}

function april() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "BS_1-4-20.%(ext)s" https://www.youtube.com/watch?v=SammgKh1MP4
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "BS_1-4-20.%(ext)s" https://www.youtube.com/watch?v=H8uVvau6b-U
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "BS_2-4-20.%(ext)s" https://www.youtube.com/watch?v=7E1rtCrRVQk
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B10_2-4-20.%(ext)s" https://www.youtube.com/watch?v=SgMw6YYAoQI
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B11_10-4-20.%(ext)s" https://www.youtube.com/watch?v=PfV92o5c_XA
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "BS_12-4-20.%(ext)s" https://www.youtube.com/watch?v=PY__r6r8OoA
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B12_17-4-20.%(ext)s" https://www.youtube.com/watch?v=GWuZ9orjoqo
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B13_24-4-20.%(ext)s" https://www.youtube.com/watch?v=rfD1iBMEaMQ
}

function may() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B14_1-5-20.%(ext)s" https://www.youtube.com/watch?v=krIf6-eW51Y
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B15_8-5-20.%(ext)s" https://www.youtube.com/watch?v=MxVg2fdxGu0
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B16_15-5-20.%(ext)s" https://www.youtube.com/watch?v=jXfyQRfWdi0
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B17_25-5-20.%(ext)s" https://www.youtube.com/watch?v=JBozPh8eNAY
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B18_29-5-20.%(ext)s" https://www.youtube.com/watch?v=3gDisdB1GDM
}

function june() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B19_5-6-20.%(ext)s" https://www.youtube.com/watch?v=On21f-btawY
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B20_12-6-20.%(ext)s" https://www.youtube.com/watch?v=AF7LVQGVTUE
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B21_19-6-20.%(ext)s" https://www.youtube.com/watch?v=q0Na5Wiyl1Y
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B22_26-6-20.%(ext)s" https://www.youtube.com/watch?v=L3hb1gI4V6E
}

function july() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B23_3-7-20.%(ext)s" https://www.youtube.com/watch?v=EIJmBCP154o
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B24_10-7-20.%(ext)s" https://www.youtube.com/watch?v=k-4aFEu_2Dw
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B25_17-7-20.%(ext)s" https://www.youtube.com/watch?v=BOqCK9k9Peo
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B26_24-7-20.%(ext)s" https://www.youtube.com/watch?v=xfc_EDyQ0to
}

function august() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B27_2-8-20.%(ext)s" https://www.youtube.com/watch?v=B2JXpf-VElM
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B28_3-8-20.%(ext)s" https://www.youtube.com/watch?v=8WMbnaTaPGM
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B29_7-8-20.%(ext)s" https://www.youtube.com/watch?v=CK769-EUIj4
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B30_14-8-20.%(ext)s" https://www.youtube.com/watch?v=MCyyp2_6NTc
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B31_21-8-20.%(ext)s" https://www.youtube.com/watch?v=Cl-Q0zOIXZQ
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B32_28-8-20.%(ext)s" https://www.youtube.com/watch?v=Fzi-Q5Q-qEc
}

function september() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B33_4-9-20.%(ext)s" https://www.youtube.com/watch?v=2P6mfalfG9k
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B34_11-9-20.%(ext)s" https://www.youtube.com/watch?v=BUchfyHmv_A
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B35_18-9-20.%(ext)s" https://www.youtube.com/watch?v=h2-5flb2cAg
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B36_25-9-20.%(ext)s" https://www.youtube.com/watch?v=i-vMnqPoRJk
}

function october() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B37_2-10-20.%(ext)s" https://www.youtube.com/watch?v=sDbQ0RB06oc
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B38_9-10-20.%(ext)s" https://www.youtube.com/watch?v=s58rEgeFbb8
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B39_16-10-20.%(ext)s" https://www.youtube.com/watch?v=1UBrcWw197w
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B40_23-10-20.%(ext)s" https://www.youtube.com/watch?v=QHPgRxP_E-A
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B41_30-10-20.%(ext)s" https://www.youtube.com/watch?v=KN9HEZeY5F0
} 

function november() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B42_6-11-20.%(ext)s" https://www.youtube.com/watch?v=BPmnRJh9eXk
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B43_13-11-20.%(ext)s" https://www.youtube.com/watch?v=WQ0gXvoyWgc
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B44_20-11-20.%(ext)s" https://www.youtube.com/watch?v=acbAwovKMIc
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B45_27-11-20.%(ext)s" https://www.youtube.com/watch?v=6rtAnLHYju0
}

function december() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B46_4-11-20.%(ext)s" https://www.youtube.com/watch?v=0nQN__ye0XM
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B47_11-11-20.%(ext)s" https://www.youtube.com/watch?v=ouKhdReUU68
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B48_16-11-20.%(ext)s" https://www.youtube.com/watch?v=Pgy35TNcLow
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B49_18-11-20.%(ext)s" https://www.youtube.com/watch?v=-y6MiDXGINM
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "BG50_25-11-20.%(ext)s" https://www.youtube.com/watch?v=kMpfyACCTeg
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B51_28-11-20.%(ext)s" https://www.youtube.com/watch?v=DT5zJKoBwoI
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "BEX51_1-1-21.%(ext)s" https://www.youtube.com/watch?v=fUt5lLB_Rf8
}	

clear 
echo Bhoutiggota 2020
echo 
echo "[1]  January"
echo "[2]  February"
echo "[3]  March"
echo "[4]  April"
echo "[5]  May"
echo "[6]  June"
echo "[7]  July"
echo "[8]  August (27-32)"
echo "[9]  September (33-36)"
echo "[10] October (37-41)"
echo "[11] November (42-45)"
echo "[12] December (46-52)"
echo "[0]  Exit"
echo 
read -p "select month : " input 
echo 

# format 
# if create_directory year month; then execute_functtion; fi
# if createDirectory 2020 January; then january; fi

if [[ $input == 1 ]]; then createDirectory 2020 January; january;
elif [[ $input == 2 ]]; then createDirectory 2020 February; february; 
elif [[ $input == 3 ]]; then createDirectory 2020 March; march; 
elif [[ $input == 4 ]]; then createDirectory 2020 April; april; 
elif [[ $input == 5 ]]; then createDirectory 2020 May; may; 
elif [[ $input == 6 ]]; then createDirectory 2020 June; june;
elif [[ $input == 7 ]]; then createDirectory 2020 July; july; 
elif [[ $input == 8 ]]; then createDirectory 2020 August; august;
elif [[ $input == 9 ]]; then createDirectory 2020 September; september;
elif [[ $input == 10 ]]; then createDirectory 2020 October; october; 
elif [[ $input == 11 ]]; then createDirectory 2020 November; november; 
elif [[ $input == 12 ]]; then createDirectory 2020 December; december;
elif [[ $input == 0 || $input == 'q' ]]; then exit 0
else echo please select between 1-12; echo; fi
exit 0
