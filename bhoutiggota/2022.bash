#!/bin/bash
#!/usr/bin/bash
#!/usr/bin/zsh

set import yt-dlp from /usr/bin/yt-dlp
set default package yt-dlp

# version=0.13 Alpha
# bhoutiggota by dr alif 

parent="https://www.youtube.com/c/Bhoutiggota/videos"
year=2022

function createDirectory() {
	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/Bhoutiggota/$year/$1; then cd Bhoot\ FM/Bhoutiggota/$year/$1; fi
	} fi
}


function january() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B106_7-1-22.%(ext)s" https://www.youtube.com/watch?v=37l1mf62m54
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B107_14-1-22.%(ext)s" https://www.youtube.com/watch?v=fovPWXQReOI
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B-BEIJUZ-2_15-1-22.%(ext)s" https://www.youtube.com/watch?v=TWMezdCBsyc
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B108_19-1-22.%(ext)s" https://www.youtube.com/watch?v=tl4m7aq5SeE
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B109_28-1-22.%(ext)s" https://www.youtube.com/watch?v=rQFApKf5nfg
}

function february() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B110_4-2-22.%(ext)s" https://www.youtube.com/watch?v=ZNLFAUZtDJ8
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B111-11-2-22.%(ext)s" https://www.youtube.com/watch?v=e_FjaY2z2vQ
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B112_18-2-22.%(ext)s" https://www.youtube.com/watch?v=REjlRqc5ETo
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B113_25-2-22.%(ext)s" https://www.youtube.com/watch?v=da8iUcnAPZY
}

function march() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B114_4-3-22.%(ext)s" https://www.youtube.com/watch?v=-yUBPy7sRAc
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B115_11-3-22.%(ext)s" https://www.youtube.com/watch?v=-tyDRuYLD8Y
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B116_18-3-22.%(ext)s" https://www.youtube.com/watch?v=B4CsQbV8rwU
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B117_25-3-22.%(ext)s" https://www.youtube.com/watch?v=Ze7SFuMTKfk
}

function april() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B118_1-4-22.%(ext)s" https://www.youtube.com/watch?v=eoJx9CVhU9A
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B119_8-4-22.%(ext)s" https://www.youtube.com/watch?v=aG40obVN4_E
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B120_15-4-22.%(ext)s" https://www.youtube.com/watch?v=89ebjNCUIQc
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B121_22-4-22.%(ext)s" https://www.youtube.com/watch?v=-koLBCOza-M
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B122_29-4-22.%(ext)s" https://www.youtube.com/watch?v=rNVjN-OPBPY
}

function may() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B123_4-5-22.%(ext)s" https://www.youtube.com/watch?v=itvq-7Tm4So
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B124_6-5-22.%(ext)s" https://www.youtube.com/watch?v=P_fXUNACCKk
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B125_13-5-22.%(ext)s" https://www.youtube.com/watch?v=vYxX_QppTm8
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B126_20-5-22.%(ext)s" https://www.youtube.com/watch?v=O3LgxGhxepQ
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B127_27-5-22.%(ext)s" https://www.youtube.com/watch?v=-IaAvVPBn2Y
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Bxxx_28-5-22.%(ext)s" https://www.youtube.com/watch?v=urMeLXYa_E8
}

function june() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B128_3-6-22.%(ext)s" https://www.youtube.com/watch?v=SduKs_bpFc4
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Bxxx_8-6-22.%(ext)s" https://www.youtube.com/watch?v=aIFJVjxv_Xk
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B129_10-6-22.%(ext)s" https://www.youtube.com/watch?v=7PlONKXZOKQ
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B130_17-6-22.%(ext)s" https://www.youtube.com/watch?v=7cbQHgayvWM&t=2s
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B131_24-6-22.%(ext)s" https://www.youtube.com/watch?v=K9b3mC8c4_0&t=6885s
}

function july() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B132_1-7-22.%(ext)s" https://www.youtube.com/watch?v=6AGSm7WyvUo
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Bxxx_5-7-22.%(ext)s" https://www.youtube.com/watch?v=BgqOKxyOkIk
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B133_8-7-22.%(ext)s" https://www.youtube.com/watch?v=c_zzXm_rB9o
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B134_11-7-22.%(ext)s" https://www.youtube.com/watch?v=ycZfsL5Udyg
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Bxxx_11-7-22.%(ext)s" https://www.youtube.com/watch?v=x_YpViiI2IU
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B135_15-7-22.%(ext)s" https://www.youtube.com/watch?v=kI7oQ4WBaFQ
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B136_22-7-22.%(ext)s" https://www.youtube.com/watch?v=5fm-0K04Jmc&t=5s
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B137_29-7-22.%(ext)s" https://www.youtube.com/watch?v=vKKtlSBCA7Y
    yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Bxxx_29-7-22.%(ext)s" https://www.youtube.com/watch?v=vKYPACzrZpQ
}

function august() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B138_5-8-22.%(ext)s" https://www.youtube.com/watch?v=yioY4XIbbgI&t=5s
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B139_12-8-22.%(ext)s" https://www.youtube.com/watch?v=_twdYMVGGn8
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B140_19-8-22.%(ext)s" https://www.youtube.com/watch?v=BABd2dtD2pE
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B141_26-8-22.%(ext)s" https://www.youtube.com/watch?v=9zx_y8NlL18
}

function september() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B142_2-9-22.%(ext)s" https://www.youtube.com/watch?v=KcseZP8WjiU
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Bxxx_7-9-22.%(ext)s" https://www.youtube.com/watch?v=qCPNqozYJRA
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B143_9-9-22.%(ext)s" https://www.youtube.com/watch?v=BcvQHTkMAIw
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Bxxx_14-9-22.%(ext)s" https://www.youtube.com/watch?v=31Oeogwisms
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B144_16-9-22.%(ext)s" https://www.youtube.com/watch?v=8dGnBejuzBU
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Bxxx_21-9-22.%(ext)s" https://www.youtube.com/watch?v=FKb_u-c4nEA
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B145_23-9-22.%(ext)s" https://www.youtube.com/watch?v=QKaBnyy9fzU
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Bxxx_28-9-22.%(ext)s" https://www.youtube.com/watch?v=3wmwI1qQdbY
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B146_30-9-22.%(ext)s" https://www.youtube.com/watch?v=YoT_f1hGquo
}

function october() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Bxxx_5-10-22.%(ext)s" https://www.youtube.com/watch?v=f9oCCtsHCF4
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B147_7-10-22.%(ext)s" https://www.youtube.com/watch?v=IM5s8oQ7fyo
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Bxxx_12-10-22.%(ext)s" https://www.youtube.com/watch?v=qb0_RvRZ-po
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B148_14-10-22.%(ext)s" https://www.youtube.com/watch?v=phBBrKeBhg0
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Bxxx_19-10-22.%(ext)s" https://www.youtube.com/watch?v=WDQd0lfjWKE
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B149_21-10-22.%(ext)s" https://www.youtube.com/watch?v=cUklqpBzJH4
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B150_28-10-22.%(ext)s" https://www.youtube.com/watch?v=FvNMuefi8Ng
}

function november() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Bxxx_2-11-22.%(ext)s" https://www.youtube.com/watch?v=MEeRpt6ZCCQ
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B151_4-11-22.%(ext)s" https://www.youtube.com/watch?v=0U0qBDywyhU
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Bxxx_9-11-22.%(ext)s" https://www.youtube.com/watch?v=aVrviZbJndI
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B152_11-11-22.%(ext)s" https://www.youtube.com/watch?v=MwcBrbidVY4
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Bxxx_16-11-22.%(ext)s" https://www.youtube.com/watch?v=9cjxnZx3p6o
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B153_18-11-22.%(ext)s" https://www.youtube.com/watch?v=eeq8BbGO2m0
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Bxxx_23-11-22.%(ext)s" https://www.youtube.com/watch?v=KymFzeDnQr0
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B154_25-11-22.%(ext)s" https://www.youtube.com/watch?v=cN3qMEeo0zQ
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Bxxx_30-11-22.%(ext)s" https://www.youtube.com/watch?v=druZ9wDVZTw
}

function december() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B155_2-12-22.%(ext)s" https://www.youtube.com/watch?v=gG44hkIzwYE
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Bxxx_7-12-22.%(ext)s" https://www.youtube.com/watch?v=5i8l1WV7yRw
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B156_9-12-22.%(ext)s" https://www.youtube.com/watch?v=wAFv0pIaBJ0
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Bxxx_14-12-22.%(ext)s" https://www.youtube.com/watch?v=TqRHELcLAzI
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B157_16-12-22.%(ext)s" https://www.youtube.com/watch?v=W2XVgiSR2QY
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Bxxx_21-12-22.%(ext)s" https://www.youtube.com/watch?v=Ze4_9Y61vVg
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B158_23-12-22.%(ext)s" https://www.youtube.com/watch?v=jZURhuJjMqs
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "Bxxx_28-12-22.%(ext)s" https://www.youtube.com/watch?v=k0TFa7Lx3hY
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B159_30-12-22.%(ext)s" https://www.youtube.com/watch?v=oct-NZLqWAU
}

clear 
echo Bhoutiggota $year
echo 
echo "[1]  January (106-109)"
echo "[2]  February (110-113)"
echo "[3]  March (114-117)"
echo "[4]  April (118-122)"
echo "[5]  May (123-127)"
echo "[6]  June (128-131)"
echo "[7]  July (132-137)"
echo "[8]  August (138-141)"
echo "[9]  September (142-146)"
echo "[10] October (147-150)"
echo "[11] November (151-154)"
echo "[12] December (155-159)"
echo "[0]  Exit"
echo 
read -p "select month : " input 
echo 

# format 
# if create_directory year month; then execute_functtion; fi
# if createDirectory 2020 January; then january; fi

if [[ $input == 1 ]]; then createDirectory January; january;
elif [[ $input == 2 ]]; then createDirectory February; february; 
elif [[ $input == 3 ]]; then createDirectory March; march; 
elif [[ $input == 4 ]]; then createDirectory April; april; 
elif [[ $input == 5 ]]; then createDirectory May; may; 
elif [[ $input == 6 ]]; then createDirectory June; june;
elif [[ $input == 7 ]]; then createDirectory July; july; 
elif [[ $input == 8 ]]; then createDirectory August; august;
elif [[ $input == 9 ]]; then createDirectory September; september;
elif [[ $input == 10 ]]; then createDirectory October; october; 
elif [[ $input == 11 ]]; then createDirectory November; november; 
elif [[ $input == 12 ]]; then createDirectory December; december;
elif [[ $input == 0 || $input == 'q' ]]; then exit 0
else echo please select between 1-12; echo; fi
exit 0

# Farhan Sadik
# Last Updated 1:38 AM 1-JAN-2023