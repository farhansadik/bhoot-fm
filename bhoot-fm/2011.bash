#!/bin/bash

set import wget 

# version=0.7 alpha
parent="http://bhoot-fm.com/archives.php#year-2011"
year=2011
dir="Bhoot FM"

# january 
function January() {

	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/$year/January; then cd Bhoot\ FM/$year/January; fi
	} fi

	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-01-14_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-01-21_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-01-28_(Bhoot-FM.com).mp3"
}

# february 
function February() {

	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/$year/February; then cd Bhoot\ FM/$year/February; fi
	} fi

	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-02-04_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-02-07_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-02-11_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-02-18_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-02-25_(Bhoot-FM.com).mp3"
}

# march
function March() {

	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/$year/March; then cd Bhoot\ FM/$year/March; fi
	} fi

	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-03-04_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-03-11_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-03-18_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-03-25_(Bhoot-FM.com).mp3"
}

# april
function April() {

	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/$year/April; then cd Bhoot\ FM/$year/April; fi
	} fi

	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-04-01_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-04-08_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-04-15_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-04-22_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-04-29_(Bhoot-FM.com).mp3"
}

# may
function May() {

	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/$year/May; then cd Bhoot\ FM/$year/May; fi
	} fi

	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-05-06_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-05-13_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-05-20_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-05-27_(Bhoot-FM.com).mp3"
}

# june 
function June() {

	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/$year/June; then cd Bhoot\ FM/$year/June; fi
	} fi

	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-06-03_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-06-10_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-06-17_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-06-24_(Bhoot-FM.com).mp3"
}

# july
function July() {

	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/$year/July; then cd Bhoot\ FM/$year/July; fi
	} fi

	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-07-01_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-07-08_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-07-15_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-07-22_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-07-29_(Bhoot-FM.com).mp3"
}

# august
function August() {

	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/$year/August; then cd Bhoot\ FM/$year/August; fi
	} fi

	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-08-05_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-08-12_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-08-19_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-08-26_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-08-31_(Bhoot-FM.com).mp3"
}

# september
function September() {

	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/$year/September; then cd Bhoot\ FM/$year/September; fi
	} fi

	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-09-09_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-09-16_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-09-30_(Bhoot-FM.com).mp3"
}



# october
function October() {

	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/$year/October; then cd Bhoot\ FM/$year/October; fi
	} fi

	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-10-14_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-10-21_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-10-28_(Bhoot-FM.com).mp3"
}

# november
function November() {

	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/$year/November; then cd Bhoot\ FM/$year/November; fi
	} fi

	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-11-04_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-11-11_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-11-18_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-11-25_(Bhoot-FM.com).mp3"
}

# december 
function December() {

	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/$year/December; then cd Bhoot\ FM/$year/December; fi
	} fi

	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-12-02_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-12-09_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-12-16_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-12-23_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2011/Bhoot-FM_2011-12-30_(Bhoot-FM.com).mp3"
}

#clear 
echo " ____   ___  _ _ 
|___ \ / _ \/ / |
  __) | | | | | |
 / __/| |_| | | |
|_____|\___/|_|_|
"

echo 
echo "[1]  January"
echo "[2]  February"
echo "[3]  March"
echo "[4]  April"
echo "[5]  May "
echo "[6]  June "
echo "[7]  July "
echo "[8]  August"
echo "[9]  September "
echo "[10] October "
echo "[11] November "
echo "[12] December "
echo "[0]  Exit"
echo 
read -p "select month : " input 
echo 

if [[ $input == 1 ]]; then January
elif [[ $input == 2 ]]; then February
elif [[ $input == 3 ]]; then March
elif [[ $input == 4 ]]; then April
elif [[ $input == 5 ]]; then May
elif [[ $input == 6 ]]; then June
elif [[ $input == 7 ]]; then July
elif [[ $input == 8 ]]; then August
elif [[ $input == 9 ]]; then September
elif [[ $input == 10 ]]; then October
elif [[ $input == 11 ]]; then November
elif [[ $input == 12 ]]; then December
elif [[ $input == 0 || $input == 'q' ]]; then exit 0
else echo please select between 1-12
echo
fi
exit 0