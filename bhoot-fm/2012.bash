#!/bin/bash

set import wget 

# version=0.7 alpha
parent="https://bhoot-fm.com/archives.php#year-2012"
year=2012

# january 
function January() {

	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/$year/January; then cd Bhoot\ FM/$year/January; fi
	} fi

	wget "http://dl.bhoot-fm.com/2012/Bhoot-FM_2012-01-06_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2012/Bhoot-FM_2012-01-13_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2012/Bhoot-FM_2012-01-20_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2012/Bhoot-FM_2012-01-27_(Bhoot-FM.com).mp3"
}

# february 
function February() {

	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/$year/February; then cd Bhoot\ FM/$year/February; fi
	} fi

	wget "http://dl.bhoot-fm.com/2012/Bhoot-FM_2012-02-03_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2012/Bhoot-FM_2012-02-10_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2012/Bhoot-FM_2012-02-17_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2012/Bhoot-FM_2012-02-24_(Bhoot-FM.com).mp3"
}

# march
function March() {

	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/$year/March; then cd Bhoot\ FM/$year/March; fi
	} fi

	wget "http://dl.bhoot-fm.com/2012/Bhoot-FM_2012-03-02_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2012/Bhoot-FM_2012-03-09_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2012/Bhoot-FM_2012-03-16_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2012/Bhoot-FM_2012-03-23_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2012/Bhoot-FM_2012-03-30_(Bhoot-FM.com).mp3"
}

# april
function April() {

	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/$year/April; then cd Bhoot\ FM/$year/April; fi
	} fi

	wget "http://dl.bhoot-fm.com/2012/Bhoot-FM_2012-04-06_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2012/Bhoot-FM_2012-04-13_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2012/Bhoot-FM_2012-04-20_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2012/Bhoot-FM_2012-04-27_(Bhoot-FM.com).mp3"
}

# may
function May() {

	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/$year/May; then cd Bhoot\ FM/$year/May; fi
	} fi

	wget "http://dl.bhoot-fm.com/2012/Bhoot-FM_2012-05-04_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2012/Bhoot-FM_2012-05-11_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2012/Bhoot-FM_2012-05-18_(Bhoot-FM.com).mp3"
	wget "http://dl.bhoot-fm.com/2012/Bhoot-FM_2012-05-25_(Bhoot-FM.com).mp3"
}

# june 
function June() {

	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/$year/June; then cd Bhoot\ FM/$year/June; fi
	} fi

	wget "http://dl.bhoot-fm.com/2012/Bhoot-FM_2012-06-01_(Bhoot-FM.com).mp3"
	echo June 8 15 22 and 29 has been skipped, due to mediafire link issue
	echo fix coming soon! 
	#wget "https://www.mediafire.com/download/gmi4katpp1qz3r5/Bhoot-FM_2012-06-08_(Bhoot-FM.com).mp3"
	#wget "https://www.mediafire.com/download/cdpkgyhunaialxu/Bhoot-FM_2012-06-15_(Bhoot-FM.com).mp3"
	#wget "https://www.mediafire.com/download/5s0re8je133tss2/Bhoot-FM_2012-06-22_(Bhoot-FM.com).mp3"
	#wget "https://www.mediafire.com/download/wjmf8vxz3pp327b/Bhoot-FM_2012-06-29_(Bhoot-FM.com).mp3"
}

# july
function July() {

	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/$year/July; then cd Bhoot\ FM/$year/July; fi
	} fi

	#wget "https://www.mediafire.com/download/3uojdfke4tlk1ei/Bhoot-FM_2012-07-06_(Bhoot-FM.com).mp3"
	#wget "https://www.mediafire.com/download/89cvzue7bz3fx59/Bhoot-FM_2012-07-13_(Bhoot-FM.com).mp3"
	#wget "https://www.mediafire.com/download/fkd154v969zvbnc/Bhoot-FM_2012-07-20_(Bhoot-FM.com).mp3"
	echo July 6 13 and 20 has been skipped, due to mediafire link issue
	echo fix coming soon! 
	wget "http://dl.bhoot-fm.com/2012/Bhoot-FM_2012-07-27_(Bhoot-FM.com).mp3"
}

clear
echo " ____   ___  _ ____  
|___ \ / _ \/ |___ \ 
  __) | | | | | __) |
 / __/| |_| | |/ __/ 
|_____|\___/|_|_____|
"

echo 
echo [1]  January
echo [2]  February 
echo [3]  March
echo [4]  April
echo [5]  May
echo [6]  June
echo [7]  July
#echo [8]  August
#echo [9]  September
#echo [10] October
#echo [11] November
#echo [12] December
echo [0]  Exit
echo 
read -p "select month : " input 
echo 

if [[ $input == 1 ]]; then January
elif [[ $input == 2 ]]; then February
elif [[ $input == 3 ]]; then March
elif [[ $input == 4 ]]; then April
elif [[ $input == 5 ]]; then May
elif [[ $input == 6 ]]; then June
elif [[ $input == 7 ]]; then July
elif [[ $input == 0 || $input == 'q' ]]; then exit 0
else echo please select between 1-7
fi


exit 0