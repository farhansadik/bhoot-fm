#!/bin/bash
#!/usr/bin/bash
#!/usr/bin/zsh

set import yt-dlp from /usr/bin/yt-dlp
set default package yt-dlp

# version=0.13 Alpha
# odvootore by ahamed babu
parent="https://www.youtube.com/c/odvootoore/videos"
year=2022

function createDirectory() {
	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/Odvootoore/$year/$1; then cd Bhoot\ FM/Odvootoore/$year/$1; fi
	} fi
}

function january() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep107_7-1-22.%(ext)s" https://www.youtube.com/watch?v=l7rKHbkt5eI
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep108_14-1-22.%(ext)s" https://www.youtube.com/watch?v=6E_9Ft0oJG4
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep109_21-1-22.%(ext)s" https://www.youtube.com/watch?v=-YOTs68VN4w
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep110_28-1-22.%(ext)s" https://www.youtube.com/watch?v=-w9wxdSnWII	
}

function february() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep111_4-2-22.%(ext)s" https://www.youtube.com/watch?v=k2pXb9WZQGY
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep112_11-2-22.%(ext)s" https://www.youtube.com/watch?v=jUhO0aUqZSY
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep113_18-2-22.%(ext)s" https://www.youtube.com/watch?v=O-KPniczqVE
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep114_25-2-22.%(ext)s" https://www.youtube.com/watch?v=cKQ0FI8BCUc
}

function march() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep115_4-3-22.%(ext)s" https://www.youtube.com/watch?v=nGTnMl7_bb8
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep116_11-3-22.%(ext)s" https://www.youtube.com/watch?v=K4PJMwh8nl4
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep117_19-3-22.%(ext)s" https://www.youtube.com/watch?v=37dN47dQKSc
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep118_25-3-22.%(ext)s" https://www.youtube.com/watch?v=pZlzxfDaSSo
}

function april() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep119_1-4-22.%(ext)s" https://www.youtube.com/watch?v=pegv3KTpOm8
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep120_8-4-22.%(ext)s" https://www.youtube.com/watch?v=JXprkoVllaY
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep121_15-4-22.%(ext)s" https://www.youtube.com/watch?v=Cg3Q6mZiwe8
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep122_22-4-22.%(ext)s" https://www.youtube.com/watch?v=ErnWPILjDAk
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep123_29-4-22.%(ext)s" https://www.youtube.com/watch?v=4TST4ePROHk
}

function may() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep124_3-5-22.%(ext)s" https://www.youtube.com/watch?v=olfb_9G53yc
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep125_13-5-22.%(ext)s" https://www.youtube.com/watch?v=_48whTtn9RI&t=932s
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep126_20-5-22.%(ext)s" https://www.youtube.com/watch?v=GCRYtTkpL08&t=4s
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep127_27-5-22.%(ext)s" https://www.youtube.com/watch?v=l_FN3uZmOjo&t=5s
}

function june() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep128_3-6-22.%(ext)s" https://www.youtube.com/watch?v=iz6Rt6qti6I&t=5s
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep129_10-6-22.%(ext)s" https://www.youtube.com/watch?v=8ACFUZr9Z_U
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep130_17-6-22.%(ext)s" https://www.youtube.com/watch?v=aNzPX6oleXc
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep131_24-6-22.%(ext)s" https://www.youtube.com/watch?v=3C2DKdJmUwU
}

function july() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep132_1-7-22.%(ext)s" https://www.youtube.com/watch?v=YycOjAHqQIU&t=1s
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep133_8-7-22.%(ext)s" https://www.youtube.com/watch?v=oM-xFjqSPpY
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep134_15-7-22.%(ext)s" https://www.youtube.com/watch?v=EfbQiFqWa5o
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep135_22-7-22.%(ext)s" https://www.youtube.com/watch?v=dOSsjD8zu8g
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep136_29-7-22.%(ext)s" https://www.youtube.com/watch?v=gY79e9qSpSs	
}

function august() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep137_5-8-22.%(ext)s" https://www.youtube.com/watch?v=2E1LMHWP-wc
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep138_12-8-22.%(ext)s" https://www.youtube.com/watch?v=QX95VpHseHU
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep139_19-8-22.%(ext)s" https://www.youtube.com/watch?v=D2xBEsePQoM
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep140_26-8-22.%(ext)s" https://www.youtube.com/watch?v=wNjMiWMpZcU
}

function september() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep141_2-9-22.%(ext)s" https://www.youtube.com/watch?v=N2wJ0Il2Jyc
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep142_9-9-22.%(ext)s" https://www.youtube.com/watch?v=jn-hR7rOhWk
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep143_16-9-22.%(ext)s" https://www.youtube.com/watch?v=bRUTGncN2nk
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep144_23-9-22.%(ext)s" https://www.youtube.com/watch?v=8rIx8lOuu9Y
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep145_30-9-22.%(ext)s" https://www.youtube.com/watch?v=43-fnWaORzk
	
}

function october() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep146_7-10-22.%(ext)s" https://www.youtube.com/watch?v=yMb34JE7aTA
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep147_14-10-22.%(ext)s" https://www.youtube.com/watch?v=qp8ieh0BiVM
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep148_21-10-22.%(ext)s" https://www.youtube.com/watch?v=SN5ZUn5VYBw
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep149_28-10-22.%(ext)s" https://www.youtube.com/watch?v=dNToaMXCRRU
}

function november() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep150_4-11-22.%(ext)s" https://www.youtube.com/watch?v=gQx0wsZOuaY
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep151_11-11-22.%(ext)s" https://www.youtube.com/watch?v=CHW2m0SGNXo
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep152_18-11-22.%(ext)s" https://www.youtube.com/watch?v=ulmqT67tF0s
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep153_25-11-22.%(ext)s" https://www.youtube.com/watch?v=yioxl4YaVEs
}

function december() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep154_2-12-22.%(ext)s" https://www.youtube.com/watch?v=tP9TD-_Jnns
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep155_9-12-22.%(ext)s" https://www.youtube.com/watch?v=DJephbkmvmE
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep156_16-12-22.%(ext)s" https://www.youtube.com/watch?v=HZTcnSb1Mco
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep157_23-12-22.%(ext)s" https://www.youtube.com/watch?v=lr2yoeM7PEM
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep158_30-12-22.%(ext)s" https://www.youtube.com/watch?v=I0pu9Yph9qw
}

clear 
echo Odvootoore 2020
echo 
echo "[1]  January (107-110)"
echo "[2]  February (110-114)"
echo "[3]  March (115-118)"
echo "[4]  April (119-123)"
echo "[5]  May (124-127)"
echo "[6]  June (128-131)"
echo "[7]  July (132-136)"
echo "[8]  August (137-140)"
echo "[9]  September (141-0145)"
echo "[10] October (146-149)"
echo "[11] November (150-153)"
echo "[12] December (154-158)"
echo "[0]  Exit"
echo 
read -p "select month : " input 
echo 

# format 
# if create_directory year month; then execute_functtion; fi
# if createDirectory 2020 January; then january; fi

if [[ $input == 1 ]]; then createDirectory January; january;
elif [[ $input == 2 ]]; then createDirectory February; february; 
elif [[ $input == 3 ]]; then createDirectory March; march; 
elif [[ $input == 4 ]]; then createDirectory April; april; 
elif [[ $input == 5 ]]; then createDirectory May; may; 
elif [[ $input == 6 ]]; then createDirectory June; june;
elif [[ $input == 7 ]]; then createDirectory July; july; 
elif [[ $input == 8 ]]; then createDirectory August; august;
elif [[ $input == 9 ]]; then createDirectory September; september;
elif [[ $input == 10 ]]; then createDirectory October; october; 
elif [[ $input == 11 ]]; then createDirectory November; november; 
elif [[ $input == 12 ]]; then createDirectory December; december;
elif [[ $input == 0 || $input == 'q' ]]; then exit 0
else echo please select between 1-12; echo; fi
exit 0
