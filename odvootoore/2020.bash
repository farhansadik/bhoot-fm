#!/bin/bash
#!/usr/bin/bash
#!/usr/bin/zsh

set import yt-dlp from /usr/bin/yt-dlp
set default package yt-dlp

# version=0.12 Alpha
# odvootore by ahamed babu
parent="https://www.youtube.com/c/odvootoore/videos"

# no protection has been implemented 
# no color has been added 
# no log system has been added 

function createDirectory() {
	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/Odvootoore/$1/$2; then cd Bhoot\ FM/Odvootoore/$1/$2; fi
	} fi
}

function january() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep2_10-1-20.%(ext)s" https://www.youtube.com/watch?v=WT6FTmgrl18
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep3_17-1-20.%(ext)s" https://www.youtube.com/watch?v=OnAgXW1SBtc
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep4_24-1-20.%(ext)s" https://www.youtube.com/watch?v=znzxT66R2-M
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep5_31-1-20.%(ext)s" https://www.youtube.com/watch?v=2RG9E7GgKno
}

function february() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep6_7-2-20.%(ext)s" https://www.youtube.com/watch?v=smS4Ci3O0lU
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep7_14-2-20.%(ext)s" https://www.youtube.com/watch?v=0pXqigXnkCg
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep8_21-2-20.%(ext)s" https://www.youtube.com/watch?v=QTZDQwnzdIY
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep9_29-2-20.%(ext)s" https://www.youtube.com/watch?v=duKrM6IfrYA

}

function march() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep10_6-3-20.%(ext)s" https://www.youtube.com/watch?v=5otGPMUVhP4
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep11_13-3-20.%(ext)s" https://www.youtube.com/watch?v=_SfoFtSmC-E
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep12_21-3-20.%(ext)s" https://www.youtube.com/watch?v=Y1A1gGFRprU
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep13_27-3-20.%(ext)s"  https://www.youtube.com/watch?v=cTa3PN4fEQQ
}

function april() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep14_3-4-20.%(ext)s" https://www.youtube.com/watch?v=VgQV4TRYyOw
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep15_11-4-20.%(ext)s" https://www.youtube.com/watch?v=5-X2gLFFxqk
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep16_17-4-20.%(ext)s" https://www.youtube.com/watch?v=dB-HqKm192c
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep17_24-4-20.%(ext)s" https://www.youtube.com/watch?v=rGG26XVyvY4
}

function may() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep18_1-5-20.%(ext)s" https://www.youtube.com/watch?v=bt3hCwoWZ4M
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep19_9-5-20.%(ext)s" https://www.youtube.com/watch?v=QD7dkOOcnoo
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep20_15-5-20.%(ext)s" https://www.youtube.com/watch?v=G9aO_XR7Aqo
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep21_22-5-20.%(ext)s" https://www.youtube.com/watch?v=5l2iUDbRS9c
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep22_26-5-20.%(ext)s" https://www.youtube.com/watch?v=fLYyqm6_Uw4
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep23_29-5-20.%(ext)s" https://www.youtube.com/watch?v=lBIc3Bp0a6U
}

function june() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep24_5-6-20.%(ext)s" https://www.youtube.com/watch?v=Cme8QVj0IEw
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep25_12-6-20.%(ext)s" https://www.youtube.com/watch?v=l1fMfhE6Ung
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep26_19-6-20.%(ext)s" https://www.youtube.com/watch?v=2ZuZ944Iw4I
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep27_26-6-20.%(ext)s" https://www.youtube.com/watch?v=PEgfvIgl034
}

function july() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep28_4-7-20.%(ext)s" https://www.youtube.com/watch?v=6Yu0fA2TS-U
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep29_10-7-20.%(ext)s" https://www.youtube.com/watch?v=iNGwRixYJV0
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep30_17-7-20.%(ext)s" https://www.youtube.com/watch?v=oQtmK2bNeRg
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep31_24-7-20.%(ext)s" https://www.youtube.com/watch?v=SVmJ7B7pzWQ
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep32_31-7-20.%(ext)s" https://www.youtube.com/watch?v=c9VMJm0B0fQ
}

function august() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep33_7-8-20.%(ext)s" https://www.youtube.com/watch?v=NJPwVWXAwcw
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep34_14-8-20.%(ext)s" https://www.youtube.com/watch?v=B2SdhbU8N_U
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep35_21-8-20.%(ext)s" https://www.youtube.com/watch?v=jktrxEsi1nM
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep36_28-8-20.%(ext)s" https://www.youtube.com/watch?v=XiklxGvGGrQ
}

function september() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep37_4-9-20.%(ext)s" https://www.youtube.com/watch?v=PI4k78y8_9E
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep38_11-9-20.%(ext)s" https://www.youtube.com/watch?v=2CKgJC-6I2o
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep39_19-9-20.%(ext)s" https://www.youtube.com/watch?v=MZGnDVvuVMw
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep40_25-9-20.%(ext)s" https://www.youtube.com/watch?v=HdRwAruz3zU
}

function october() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep41_2-10-20.%(ext)s" https://www.youtube.com/watch?v=xkkoEvSEPPM
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep42_9-10-20.%(ext)s" https://www.youtube.com/watch?v=lfrfcO6Ir0o
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep43_16-10-20.%(ext)s" https://www.youtube.com/watch?v=TLx9Bi37Cqo
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep44_23-10-20.%(ext)s" https://www.youtube.com/watch?v=qmJyghDhHiU
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep45_30-10-20.%(ext)s" https://www.youtube.com/watch?v=LbqYkA4Qf6Y
}

function november() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep46_6-11-20.%(ext)s" https://www.youtube.com/watch?v=ch9Apk7eEIw
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep47_13-11-20.%(ext)s" https://www.youtube.com/watch?v=ptvze-4gMhU
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep48_20-11-20.%(ext)s" https://www.youtube.com/watch?v=Bhubi8PuiIs
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep49_27-11-20.%(ext)s" https://www.youtube.com/watch?v=A17nI5a5VpY
}

function december() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep50_4-12-20.%(ext)s" https://www.youtube.com/watch?v=9wpD7fRsfVg
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep51_11-12-20.%(ext)s" https://www.youtube.com/watch?v=luoQFEjL5CM
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep52_19-12-20.%(ext)s" https://www.youtube.com/watch?v=r7eFu11VOXE
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep53_25-12-20.%(ext)s" https://www.youtube.com/watch?v=_ZFAlWIIWn0
}

clear 
echo Odvootoore 2020
echo 
echo "[1]  January"
echo "[2]  February"
echo "[3]  March"
echo "[4]  April"
echo "[5]  May"
echo "[6]  June"
echo "[7]  July"
echo "[8]  August"
echo "[9]  September (37-40)"
echo "[10] October (41-45)"
echo "[11] November (46-49)"
echo "[12] December (50-53)"
echo "[0]  Exit"
echo 
read -p "select month : " input 
echo 

# format 
# if create_directory year month; then execute_functtion; fi
# if createDirectory 2020 January; then january; fi

if [[ $input == 1 ]]; then createDirectory 2020 January; january;
elif [[ $input == 2 ]]; then createDirectory 2020 February; february; 
elif [[ $input == 3 ]]; then createDirectory 2020 March; march; 
elif [[ $input == 4 ]]; then createDirectory 2020 April; april; 
elif [[ $input == 5 ]]; then createDirectory 2020 May; may; 
elif [[ $input == 6 ]]; then createDirectory 2020 June; june;
elif [[ $input == 7 ]]; then createDirectory 2020 July; july; 
elif [[ $input == 8 ]]; then createDirectory 2020 August; august;
elif [[ $input == 9 ]]; then createDirectory 2020 September; september;
elif [[ $input == 10 ]]; then createDirectory 2020 October; october; 
elif [[ $input == 11 ]]; then createDirectory 2020 November; november; 
elif [[ $input == 12 ]]; then createDirectory 2020 December; december;
elif [[ $input == 0 || $input == 'q' ]]; then exit 0
else echo please select between 1-12; echo; fi
exit 0
