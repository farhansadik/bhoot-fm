#!/bin/bash
#!/usr/bin/bash
#!/usr/bin/zsh

set import yt-dlp from /usr/bin/yt-dlp
set default package yt-dlp

# version=0.13 Alpha
# odvootore by ahamed babu
parent="https://www.youtube.com/c/odvootoore/videos"
year=2024

# no protection has been implemented 
# no color has been added 
# no log system has been added 

function createDirectory() {
	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/Odvootoore/$year/$1; then cd Bhoot\ FM/Odvootoore/$year/$1; fi
	} fi
}

function sampleFunction() {
	
	# File Name Format
	# if 'special episode' = BS_DD_MM_YY
	# if 'basic episode' = epX_DD_MM_YY

	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep2_10-1-20.%(ext)s" https://www.youtube.com/watch?v=WT6FTmgrl18
}

function january() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep211_5-1-24.%(ext)s" https://www.youtube.com/watch?v=qFgI6j7drHg
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep212_12-1-24.%(ext)s" https://www.youtube.com/watch?v=yixjt-eKIDE
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep213_19-1-24.%(ext)s" https://www.youtube.com/watch?v=aCOpWSMjWgE
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep214_26-1-24.%(ext)s" https://www.youtube.com/watch?v=rb5xCJE82O0
}

function february() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep215_2-2-24.%(ext)s" https://www.youtube.com/watch?v=HenvfvMd1CI
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep216_9-2-24.%(ext)s" https://www.youtube.com/watch?v=fS5jnsVXYbE
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep217_16-2-24.%(ext)s" https://www.youtube.com/watch?v=5r4MYUH_7J4
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep218_23-2-24.%(ext)s" https://www.youtube.com/watch?v=i_fXMeqc1yQ
}

function march() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep219_1-3-24.%(ext)s" https://www.youtube.com/watch?v=qitpFOb3-YE
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep220_8-3-24.%(ext)s" https://www.youtube.com/watch?v=oxuIEmfv6XM
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep221_15-3-24.%(ext)s" https://www.youtube.com/watch?v=KcwQev2Cqsw
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep222_22-3-24.%(ext)s" https://www.youtube.com/watch?v=RKU_gQTPFeY
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep223_29-3-24.%(ext)s" https://www.youtube.com/watch?v=EtZpafJ3aDQ
}

function april() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep224_5-4-24.%(ext)s" https://www.youtube.com/watch?v=oHGiExouO6s;
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep225_12-4-24.%(ext)s" https://www.youtube.com/watch?v=zcxFbE8fyE8;
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep226_19-4-24.%(ext)s" https://www.youtube.com/watch?v=iSf7wVxeCfs;
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep227_26-4-24.%(ext)s" https://www.youtube.com/watch?v=ukO3xX_Cmg8;
}

function may() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep228_3-5-24.%(ext)s" https://www.youtube.com/watch?v=foU2UUzDH5g;
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep229_10-5-24.%(ext)s" https://www.youtube.com/watch?v=gJA05vmJmmE;
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep230_17-5-24.%(ext)s" https://www.youtube.com/watch?v=ohLvCjGzpHY;
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep231_24-5-24.%(ext)s" https://www.youtube.com/watch?v=yFGnZ1gw8DQ;
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep232_31-5-24.%(ext)s" https://www.youtube.com/watch?v=BDaVLkaJx4k;
}

function june() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep233_7-6-24.%(ext)s" https://www.youtube.com/watch?v=nJPbzs5M1YQ
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep234_14-6-24.%(ext)s" https://www.youtube.com/watch?v=XRCXv0PCnhs
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep235_21-6-24.%(ext)s" https://www.youtube.com/watch?v=9cQTHyyuKkk
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep236_28-6-24.%(ext)s" https://www.youtube.com/watch?v=KXhSgK0Y7O4
}

function july() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep237_5-7-24.%(ext)s" https://www.youtube.com/watch?v=zv9quB0vYLs
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep238_12-7-24.%(ext)s" https://www.youtube.com/watch?v=XCX_yUJMo8I
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep239_26-7-24.%(ext)s" https://www.youtube.com/watch?v=k_R8Ev7GQXI

}

function august() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep240_2-8-24.%(ext)s" https://www.youtube.com/watch?v=VhLM4xW7cy0
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep241_10-8-24.%(ext)s" https://www.youtube.com/watch?v=gffUbATJGbw
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep242_16-8-24.%(ext)s" https://www.youtube.com/watch?v=k_Aagqph9x0
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep243_23-8-24.%(ext)s" https://www.youtube.com/watch?v=dphmiPnrHW0
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep244_30-8-24.%(ext)s" https://www.youtube.com/watch?v=rI26074uTEc
}

function september() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep245_6-9-24.%(ext)s" https://www.youtube.com/watch?v=9btPegD5ujU
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep246_13-9-24.%(ext)s" https://www.youtube.com/watch?v=EQcKEKGwidQ
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep247_20-9-24.%(ext)s" https://www.youtube.com/watch?v=WPD2O-KLLWo
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep248_27-9-24.%(ext)s" https://www.youtube.com/watch?v=DxFpjJk7A8c 
}

function october() {
	sleep 1
}

function november() {
	sleep 1
}

function december() {
	sleep 1
}

clear 
echo Odvootoore $year
echo 
echo "[1]  January (211-214)";
echo "[2]  February (215-218)";
echo "[3]  March (219-223)";
echo "[4]  April (224-227)";
echo "[5]  May (228-232)";
echo "[6]  June (233-236)"
echo "[7]  July (237-239)"
echo "[8]  August (240-244)"
echo "[9]  September (245-248)"
#echo "[10] October ()"
#echo "[11] November ()"
#echo "[12] December ()"
echo "[0]  Exit"
echo 
read -p "select month : " input ;
echo 

# format 
# if create_directory year month; then execute_functtion; fi
# if createDirectory 2020 January; then january; fi

if [[ $input == 1 ]]; then createDirectory January; january;
elif [[ $input == 2 ]]; then createDirectory February; february; 
elif [[ $input == 3 ]]; then createDirectory March; march; 
elif [[ $input == 4 ]]; then createDirectory April; april; 
elif [[ $input == 5 ]]; then createDirectory May; may; 
elif [[ $input == 6 ]]; then createDirectory June; june;
elif [[ $input == 7 ]]; then createDirectory July; july; 
elif [[ $input == 8 ]]; then createDirectory August; august;
elif [[ $input == 9 ]]; then createDirectory September; september;
elif [[ $input == 10 ]]; then createDirectory October; october; 
elif [[ $input == 11 ]]; then createDirectory November; november; 
elif [[ $input == 12 ]]; then createDirectory December; december;
elif [[ $input == 0 || $input == 'q' ]]; then exit 0
else echo please select between 1-12; echo; fi
exit 0