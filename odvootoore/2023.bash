#!/bin/bash
#!/usr/bin/bash
#!/usr/bin/zsh

set import yt-dlp from /usr/bin/yt-dlp
set default package yt-dlp

# version=0.13 Alpha
# odvootore by ahamed babu
parent="https://www.youtube.com/c/odvootoore/videos"
year=2023

# no protection has been implemented 
# no color has been added 
# no log system has been added 

function createDirectory() {
	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/Odvootoore/$year/$1; then cd Bhoot\ FM/Odvootoore/$year/$1; fi
	} fi
}

function january() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep159_6-1-23.%(ext)s" https://www.youtube.com/watch?v=8Jsq1IyVkn8
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep160_13-1-23.%(ext)s" https://www.youtube.com/watch?v=C6MP4rf-qcs
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep161_20-1-23.%(ext)s" https://www.youtube.com/watch?v=p8vtwXQqAzU
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep162_27-1-23.%(ext)s" https://www.youtube.com/watch?v=MGg2Kh04XG4
}

function february() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep163_3-2-23.%(ext)s" https://www.youtube.com/watch?v=sYLTiANI0jw
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep164_10-2-23.%(ext)s" https://www.youtube.com/watch?v=T5IsAz7K7Q4
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep165_17-2-23.%(ext)s" https://www.youtube.com/watch?v=hnXCmQ4Nru4
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep166_24-2-23.%(ext)s" https://www.youtube.com/watch?v=A6tpSmUHvT4
}

function march() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep167_3-3-23.%(ext)s" https://www.youtube.com/watch?v=-jeNqYoIB18
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep168_10-3-23.%(ext)s" https://www.youtube.com/watch?v=biFm2Cqq9yw
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep169_24-3-23.%(ext)s" https://www.youtube.com/watch?v=iA7zpUwOhoo
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep170_24-3-23.%(ext)s" https://www.youtube.com/watch?v=m2pG01qhjX8
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep170_24-3-23.%(ext)s" https://www.youtube.com/watch?v=m2pG01qhjX8
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep171_31-3-23.%(ext)s" https://www.youtube.com/watch?v=wQMr5mQNslA
}

function april() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep172_7-4-23.%(ext)s" https://www.youtube.com/watch?v=29qhKAo6Spc
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep173_15-4-23.%(ext)s" https://www.youtube.com/watch?v=4V8NSQrDiIg
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep174_21-4-23.%(ext)s" https://www.youtube.com/watch?v=pxk2a8Wih04
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep175_28-4-23.%(ext)s" https://www.youtube.com/watch?v=Dr0ql4UkuTc
}

function may() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep176_5-5-23.%(ext)s" https://www.youtube.com/watch?v=RJSgNYm-Zpg
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep177_12-5-23.%(ext)s" https://www.youtube.com/watch?v=luwqlgd8kjY
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep178_19-5-23.%(ext)s" https://www.youtube.com/watch?v=bjjUeJGT_ec
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep179_26-5-23.%(ext)s" https://www.youtube.com/watch?v=eAu1hZ3TYdU
}

function june() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep180_3-6-23.%(ext)s" https://www.youtube.com/watch?v=E2kA57p7TGg
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep181_9-6-23.%(ext)s" https://www.youtube.com/watch?v=YZAIUjsMnhQ
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep182_16-6-23.%(ext)s" https://www.youtube.com/watch?v=a-bocw5XuFw
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep183_23-6-23.%(ext)s" https://www.youtube.com/watch?v=xLfNtY1gKU0
}

function july() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep184_1-7-23.%(ext)s" https://www.youtube.com/watch?v=sSesn2mWJPg
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep185_7-7-23.%(ext)s" https://www.youtube.com/watch?v=1bVbnlBR650
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep186_14-7-23.%(ext)s" https://www.youtube.com/watch?v=EArXFHU0JDM
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep187_21-7-23.%(ext)s" https://www.youtube.com/watch?v=kG8gILhAuMo
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep188_29-7-23.%(ext)s" https://www.youtube.com/watch?v=A3lofwQMeuE
}

function august() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep189_4-8-23.%(ext)s" https://www.youtube.com/watch?v=7wihjClmtHk
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep190_11-8-23.%(ext)s" https://www.youtube.com/watch?v=OYUTfe1Yzso
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep191_18-8-23.%(ext)s" https://www.youtube.com/watch?v=52DG_y_rVXc
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep192_25-8-23.%(ext)s" https://www.youtube.com/watch?v=PlOipeD17vg
}

function september() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep193_1-9-23.%(ext)s" https://www.youtube.com/watch?v=6he0jj4g_DU
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep194_8-9-23.%(ext)s" https://www.youtube.com/watch?v=CDtCjjWxgK0
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep195_15-9-23.%(ext)s" https://www.youtube.com/watch?v=14n-gTwP154
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep196_22-9-23.%(ext)s" https://www.youtube.com/watch?v=H-Z3ukzkvyw
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep197_29-9-23.%(ext)s" https://www.youtube.com/watch?v=cSrxt0iaTN4
}

function october() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep198_6-10-23.%(ext)s" https://www.youtube.com/watch?v=bBVfYGCWWpw
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep199_13-10-23.%(ext)s" https://www.youtube.com/watch?v=qV-8MOX6Uuo
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep200_20-10-23.%(ext)s" https://www.youtube.com/watch?v=iz1m0CO8l5w
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep201_27-10-23.%(ext)s" https://www.youtube.com/watch?v=yu2XLPJ1AeE
}

function november() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep202_3-11-23.%(ext)s" https://www.youtube.com/watch?v=1UE4vnajdPc
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep203_10-11-23.%(ext)s" https://www.youtube.com/watch?v=xxD19h5EDE4
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep204_17-11-23.%(ext)s" https://www.youtube.com/watch?v=_9umYwD1BtE
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep205_24-11-23.%(ext)s" https://www.youtube.com/watch?v=CBapMnW7RKc
}

function december() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep206_1-12-23.%(ext)s" https://www.youtube.com/watch?v=1zdRIXBWVi8
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep207_8-12-23.%(ext)s" https://www.youtube.com/watch?v=MO8aIYzTdpw
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep208_15-12-23.%(ext)s" https://www.youtube.com/watch?v=EZoycSiGxLQ
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep209_22-12-23.%(ext)s" https://www.youtube.com/watch?v=EZoycSiGxLQ
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep210_29-12-23.%(ext)s" https://www.youtube.com/watch?v=oVLREDKZtgA
}

clear 
echo Odvootoore $year
echo 
echo "[1]  January (159-162)"
echo "[2]  February (163-166)"
echo "[3]  March (167-171)"
echo "[4]  April (172-175)"
echo "[5]  May (175-179)"
echo "[6]  June (180-183)"
echo "[7]  July (184-188)"
echo "[8]  August (189-192)"
echo "[9]  September (193-197)"
echo "[10] October (198-201)"
echo "[11] November (202-205)"
echo "[12] December (206-210)"
echo "[0]  Exit"
echo 
read -p "select month : " input 
echo 

# format 
# if create_directory year month; then execute_functtion; fi
# if createDirectory 2020 January; then january; fi

if [[ $input == 1 ]]; then createDirectory January; january;
elif [[ $input == 2 ]]; then createDirectory February; february; 
elif [[ $input == 3 ]]; then createDirectory March; march; 
elif [[ $input == 4 ]]; then createDirectory April; april; 
elif [[ $input == 5 ]]; then createDirectory May; may; 
elif [[ $input == 6 ]]; then createDirectory June; june;
elif [[ $input == 7 ]]; then createDirectory July; july; 
elif [[ $input == 8 ]]; then createDirectory August; august;
elif [[ $input == 9 ]]; then createDirectory September; september;
elif [[ $input == 10 ]]; then createDirectory October; october; 
elif [[ $input == 11 ]]; then createDirectory November; november; 
elif [[ $input == 12 ]]; then createDirectory December; december;
elif [[ $input == 0 || $input == 'q' ]]; then exit 0
else echo please select between 1-12; echo; fi
exit 0