#!/bin/bash
#!/usr/bin/bash
#!/usr/bin/zsh

set import yt-dlp from /usr/bin/yt-dlp
set default package yt-dlp

# version=0.14 Alpha
# odvootore by ahamed babu
parent="https://www.youtube.com/c/odvootoore/videos"
year=2021
# no protection has been implemented 
# no color has been added 
# no log system has been added 

function createDirectory() {
	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/Odvootoore/$year/$1; then cd Bhoot\ FM/Odvootoore/$year/$1; fi
	} fi
}

function january() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep54_1-1-21.%(ext)s" https://www.youtube.com/watch?v=Qsnne_3bxxU
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep55_8-1-21.%(ext)s" https://www.youtube.com/watch?v=gs2h_VAyVrY
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep56_15-1-21.%(ext)s" https://www.youtube.com/watch?v=ElkT8BTRH6A
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep57_22-1-21.%(ext)s" https://www.youtube.com/watch?v=91sHEvc6Kgc
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep58_29-1-21.%(ext)s" https://www.youtube.com/watch?v=vC_iOdc6Eyc
}

function february() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep59_5-2-21.%(ext)s" https://www.youtube.com/watch?v=o-rFyzEegis
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep60_12-2-21.%(ext)s" https://www.youtube.com/watch?v=Jq2xKGQv4lM
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep61_19-2-21.%(ext)s" https://www.youtube.com/watch?v=TDy25pIhEP4
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep62_26-2-21.%(ext)s" https://www.youtube.com/watch?v=O5SObf2H_sw
}

function march() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep63_5-3-21.%(ext)s" https://www.youtube.com/watch?v=K4OdFj6WwK4
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep64_12-3-21.%(ext)s" https://www.youtube.com/watch?v=FRLqDOR6Pvs
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep65_19-3-21.%(ext)s" https://www.youtube.com/watch?v=tkVvB3wOy6Y
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep66_26-3-21.%(ext)s" https://www.youtube.com/watch?v=BPNXQ4axOug
}

function april() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep67_2-4-21.%(ext)s" https://www.youtube.com/watch?v=kNIQdJPymgE
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep68_9-4-21.%(ext)s" https://www.youtube.com/watch?v=IleDi5IUMXw
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep69_16-4-21.%(ext)s" https://www.youtube.com/watch?v=H9WNAPX9cCs
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep70_23-4-21.%(ext)s" https://www.youtube.com/watch?v=fSardeF1hdU
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep71_30-4-21.%(ext)s" https://www.youtube.com/watch?v=HGA605Whxtw
}

function may() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep72_7-5-21.%(ext)s" https://www.youtube.com/watch?v=bsgGSozLfrE
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep73_14-5-21.%(ext)s" https://www.youtube.com/watch?v=rnd6Eq1hpv8
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep74_21-5-21.%(ext)s" https://www.youtube.com/watch?v=n-1BDRWAvlM
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep75_29-5-21.%(ext)s" https://www.youtube.com/watch?v=ZN3woKEXIXE
}

function june() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep76_4-6-21.%(ext)s" https://www.youtube.com/watch?v=q6CzCQ4hylQ
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep77_11-6-21.%(ext)s" https://www.youtube.com/watch?v=Jn7hHoD8oZ4
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep78_18-6-21.%(ext)s" https://www.youtube.com/watch?v=tF7jFbdOT9w
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep79_25-6-21.%(ext)s" https://www.youtube.com/watch?v=EDfGiAegLlA
}

function july() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep80_2-7-21.%(ext)s" https://www.youtube.com/watch?v=ovlLDOKbazI
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep81_9-7-21.%(ext)s" https://www.youtube.com/watch?v=EstpYAmLOaY
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep82_16-7-21.%(ext)s" https://www.youtube.com/watch?v=yML_orCKCK8
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep83_23-7-21.%(ext)s" https://www.youtube.com/watch?v=IYpk4ajUAxs
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep84_30-7-21.%(ext)s" https://www.youtube.com/watch?v=N5Vxlp3l5vc
}

function august() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep85_6-8-21.%(ext)s" https://www.youtube.com/watch?v=LtsN5tqt178
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep86_13-8-21.%(ext)s" https://www.youtube.com/watch?v=JVzrNtrJyy8
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep87_20-8-21.%(ext)s" https://www.youtube.com/watch?v=uVweKrRfHJw
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep88_27-8-21.%(ext)s" https://www.youtube.com/watch?v=9NE6FmziJvg
}

function september() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep89_3-9-21.%(ext)s" https://www.youtube.com/watch?v=c2xVPTRWvuM
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep90_10-9-21.%(ext)s" https://www.youtube.com/watch?v=CmV9pEZ6kWg
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep91_17-9-21.%(ext)s" https://www.youtube.com/watch?v=jEur1duigkY
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep92_24-9-21.%(ext)s" https://www.youtube.com/watch?v=Fi0bOQSlf_8
}

function october() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep93_1-10-21.%(ext)s" https://www.youtube.com/watch?v=_3E70qm0AHA
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep94_8-10-21.%(ext)s" https://www.youtube.com/watch?v=JAUh9t5UVoI
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep95_15-10-21.%(ext)s" https://www.youtube.com/watch?v=lmPooV_VeLg
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep96_22-10-21.%(ext)s" https://www.youtube.com/watch?v=4eBOBTSeCv4
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep97_29-10-21.%(ext)s" https://www.youtube.com/watch?v=e1Qli0L79q0
}

function november() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep98_5-11-21.%(ext)s" https://www.youtube.com/watch?v=zYTIByAp-0k
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep99_12-11-21.%(ext)s" https://www.youtube.com/watch?v=Qk2-ORIp5yo
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep100_19-11-21.%(ext)s" https://www.youtube.com/watch?v=w1NFBz9Fypw
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep101_26-11-21.%(ext)s" https://www.youtube.com/watch?v=jyo4FAD7Dr0
}

function december() {
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep102_3-12-21.%(ext)s" https://www.youtube.com/watch?v=HNwRgcM_lck
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep103_10-12-21.%(ext)s" https://www.youtube.com/watch?v=sRqLoo-c2Z0
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep104_17-12-21.%(ext)s" https://www.youtube.com/watch?v=o9ZWgekgIzw
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep105_24-12-21.%(ext)s" https://www.youtube.com/watch?v=G-UgRbUEYnU
	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "ep106_31-12-21.%(ext)s" https://www.youtube.com/watch?v=HpnwNyiWhoA
}

clear 
echo Odvootoore 2021
echo 
echo "[1]  January (54-58)"
echo "[2]  February (59-62)"
echo "[3]  March (63-66)"
echo "[4]  April (67-71)"
echo "[5]  May (72-75)"
echo "[6]  June (76-79)"
echo "[7]  July (82-84)"
echo "[8]  August (85-88)"
echo "[9]  September (89-92)"
echo "[10] October (93-97)"
echo "[11] November (98-101)"
echo "[12] December (102-106)"
echo "[0]  Exit"
echo 
read -p "select month : " input 
echo 

# format 
# if create_directory year month; then execute_functtion; fi
# if createDirectory 2020 January; then january; fi

if [[ $input == 1 ]]; then createDirectory January; january;
elif [[ $input == 2 ]]; then createDirectory February; february; 
elif [[ $input == 3 ]]; then createDirectory March; march; 
elif [[ $input == 4 ]]; then createDirectory April; april; 
elif [[ $input == 5 ]]; then createDirectory May; may; 
elif [[ $input == 6 ]]; then createDirectory June; june;
elif [[ $input == 7 ]]; then createDirectory July; july; 
elif [[ $input == 8 ]]; then createDirectory August; august;
elif [[ $input == 9 ]]; then createDirectory September; september;
elif [[ $input == 10 ]]; then createDirectory October; october; 
elif [[ $input == 11 ]]; then createDirectory November; november; 
elif [[ $input == 12 ]]; then createDirectory December; december;
elif [[ $input == 0 || $input == 'q' ]]; then exit 0
else echo please select between 1-12; echo; fi
exit 0
