#!/bin/bash

set import wget 

# version=0.7 alpha
parent="http://bhoot-fm.com/archives.php#year-2011"
year=2011

# january 
function January() {

	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/$year/January; then cd Bhoot\ FM/$year/January; fi
	} fi

	wget ""
}

# february 
function February() {

	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/$year/February; then cd Bhoot\ FM/$year/February; fi
	} fi

	wget ""
}

# march
function March() {

	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/$year/March; then cd Bhoot\ FM/$year/March; fi
	} fi

	wget ""
}

# april
function April() {

	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/$year/April; then cd Bhoot\ FM/$year/April; fi
	} fi

	wget ""
}

# may
function May() {

	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/$year/May; then cd Bhoot\ FM/$year/May; fi
	} fi

	wget ""
}

# june 
function June() {

	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/$year/June; then cd Bhoot\ FM/$year/June; fi
	} fi

	wget ""
}

# july
function July() {

	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/$year/July; then cd Bhoot\ FM/$year/July; fi
	} fi

	wget ""
}

# august
function August() {

	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/$year/August; then cd Bhoot\ FM/$year/August; fi
	} fi

	wget ""
}

# september
function September() {

	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/$year/September; then cd Bhoot\ FM/$year/September; fi
	} fi
	
	wget ""
}



# october
function October() {

	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/$year/October; then cd Bhoot\ FM/$year/October; fi
	} fi

	wget ""
}

# november
function November() {

	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/$year/November; then cd Bhoot\ FM/$year/November; fi
	} fi

	wget ""
}

# december 
function December() {

	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/$year/December; then cd Bhoot\ FM/$year/December; fi
	} fi

	wget ""
}

#clear 
echo " ____   ___  _ _ 
|___ \ / _ \/ / |
  __) | | | | | |
 / __/| |_| | | |
|_____|\___/|_|_|
"

echo 
echo [1]  January
echo [2]  February 
echo [3]  March
echo [4]  April
echo [5]  May
echo [6]  June
echo [7]  July
echo [8]  August
echo [9]  September
echo [10] October
echo [11] November
echo [12] December
echo [0]  Exit
echo 
read -p "select month : " input 
echo 

if [[ $input == 1 ]]; then January
elif [[ $input == 2 ]]; then February
elif [[ $input == 3 ]]; then March
elif [[ $input == 4 ]]; then April
elif [[ $input == 5 ]]; then May
elif [[ $input == 6 ]]; then June
elif [[ $input == 7 ]]; then July
elif [[ $input == 8 ]]; then August
elif [[ $input == 9 ]]; then September
elif [[ $input == 10 ]]; then October
elif [[ $input == 11 ]]; then November
elif [[ $input == 12 ]]; then December
elif [[ $input == 0 || $input == 'q' ]]; then exit 0
else echo please select between 1-7
echo
fi
exit 0