#!/bin/bash
#!/usr/bin/bash
#!/usr/bin/zsh

set import yt-dlp from /usr/bin/yt-dlp
set default package yt-dlp

# version=0.13 Alpha
# bhoutiggota by dr alif 
parent="https://www.youtube.com/c/Bhoutiggota/videos"
year=2020

# no protection has been implemented 
# no color has been added 
# no log system has been added 

function createDirectory() {
	if cd $HOME/Desktop; then {
		if mkdir -p Bhoot\ FM/Bhoutiggota/$year/$1; then cd Bhoot\ FM/Bhoutiggota/$year/$1; fi
	} fi
}

function sampleFunction() {
	
	# File Name Format
	# if 'special episode' = BS_DD_MM_YY
	# if 'basic episode' = epX_DD_MM_YY

	yt-dlp -f 'ba' -x --audio-format mp3 --no-keep-video --output "B1_30-1-20.%(ext)s" https://www.youtube.com/watch?v=ymYB4d6_w9E
}

function january() {
	sleep 1
}

function february() {
	sleep 1
}

function march() {
	sleep 1
}

function april() {
	sleep 1
}

function may() {
	sleep 1
}

function june() {
	sleep 1
}

function july() {
	sleep 1
}

function august() {
	sleep 1
}

function september() {
	sleep 1
}

function october() {
	sleep 1
}

function november() {
	sleep 1
}

function december() {
	sleep 1
}


clear 
echo Bhoutiggota $year
echo 
echo "[1]  January ()"
echo "[2]  February ()"
echo "[3]  March ()"
echo "[4]  April ()"
echo "[5]  May ()"
echo "[6]  June ()"
echo "[7]  July ()"
echo "[8]  August ()"
echo "[9]  September ()"
echo "[10] October ()"
echo "[11] November ()"
echo "[12] December ()"
echo "[0]  Exit"
echo 
read -p "select month : " input 
echo 

# format 
# if create_directory year month; then execute_functtion; fi
# if createDirectory 2020 January; then january; fi

if [[ $input == 1 ]]; then createDirectory January; january;
elif [[ $input == 2 ]]; then createDirectory February; february; 
elif [[ $input == 3 ]]; then createDirectory March; march; 
elif [[ $input == 4 ]]; then createDirectory April; april; 
elif [[ $input == 5 ]]; then createDirectory May; may; 
elif [[ $input == 6 ]]; then createDirectory June; june;
elif [[ $input == 7 ]]; then createDirectory July; july; 
elif [[ $input == 8 ]]; then createDirectory August; august;
elif [[ $input == 9 ]]; then createDirectory September; september;
elif [[ $input == 10 ]]; then createDirectory October; october; 
elif [[ $input == 11 ]]; then createDirectory November; november; 
elif [[ $input == 12 ]]; then createDirectory December; december;
elif [[ $input == 0 || $input == 'q' ]]; then exit 0
else echo please select between 1-12; echo; fi
exit 0